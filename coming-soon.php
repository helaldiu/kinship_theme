<?php
/*
* Template Name: Coming Soon Page
*/
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Kinship: Factory Workforce Management Platform</title>
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri();?>/assets/images/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri();?>/assets/images/favicon/favicon-16x16.png" sizes="16x16" />

    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" 
    integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">

    <style>
        #comingSoon {
            width: 100vw;
            height: 100vh;
            position: relative;
            background-color: #fce4e1;
            padding: 6rem 0;
        }
    </style>
</head>

<body>
    <section id="comingSoon" class="d-flex align-items-center justify-content-center">
        <div class="container relative">
            <div class="row">
                <div class="col-md-6 col-lg-6 mx-auto text-center">
                    <h1 class="hero-heading">
                        <img src="<?php echo get_template_directory_uri();?>/assets/images/logo-icon.svg" alt="Kinship" style="max-width: 200px;">
                    </h1>
                </div>
            </div>
        </div>
    </section>
</body>

</html>