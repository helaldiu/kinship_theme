<?php

add_action('admin_init', 'custom_theme_options', 1);

function custom_theme_options() {



    $saved_settings = get_option('option_tree_settings', array());





    $custom_settings = array(

        'sections' => array(
            array(
                'id' => 'init',
                'title' => 'General Settings '
            ),
            array(
                'id' => 'page-settings',
                'title' => 'Page Settings'
            ),
            array(
                'id' => 'contentpanel',
                'title' => 'Content Panel'
            )

        ),
        'settings' => array(
            array(
                'id'          => 'login_page',
                'label'       => __( 'Set Login Page', 'kinship' ),
                'desc'        => __( 'select page that you created for user login', 'kinship' ),
                'type'        => 'page-select',
                'section'     => 'page-settings',
            ),
            array(
                'id'          => 'rd_page',
                'label'       => __( 'Set Demo Request Page', 'kinship' ),
                'desc'        => __( 'select page that contains demo request form', 'kinship' ),
                'type'        => 'page-select',
                'section'     => 'page-settings',
            ),
		
            array(
                'id' => 'logo',
                'label' => 'LOGO',
                'desc' => '',
                'type' => 'upload',
                'section' => 'init'
            ),array(
                'id' => 'logo_gray',
                'label' => 'LOGO Grayscale',
                'desc' => '',
                'type' => 'upload',
                'section' => 'init'
            ),array(
                'id' => 'home_banner_title',
                'label' => 'home_banner_title',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            ),array(
                'id' => 'home_banner_subtitle',
                'label' => 'Home Banner SubTitle',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            ),array(
                'id' => 'home_banner_photo',
                'label' => 'Home Banner Photo',
                'desc' => '',
                'type' => 'upload',
                'section' => 'init'
            ),array(
                'id' => 'home_banner_slider',
                'label' => 'Home Banner Slider',
                'desc' => '',
                'type' => 'upload',
                'section' => 'init',
				'type' => 'list-item',
                    'class' => '',
                    'settings' => array(
                       array(
                            'label' => 'Image',
                            'id' => 'image',
                            
                            'type' => 'upload',
                            
                        )
                    )
            ),array(
                'id' => 'home_about_title',
                'label' => 'Home About Title',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            ), array(
                'id' => 'home_page_about_section',
                'label' => 'Home About Section',
                'desc' => '',
				 'section' => 'init',
                'type' => 'list-item',
                    'class' => '',
                    'settings' => array(
                        array(
                            'label' => 'Description',
                            'id' => 'desc',
                            
                            'type' => 'text',
                            
                        ),array(
                            'label' => 'Image',
                            'id' => 'image',
                            
                            'type' => 'upload',
                            
                        )
                    )
            ),array(
                'id' => 'home_benifits_title',
                'label' => 'Home Benifits Section Title',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            ),array(
                'id' => 'home_benifits_subtitle',
                'label' => 'Home Benifits Section Subtitle',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            ), array(
                'id' => 'home_page_benifits_section',
                'label' => 'Home Benifits Section',
                'desc' => '',
                 'section' => 'init',
                'type' => 'list-item',
                    'class' => '',
                    'settings' => array(
                        array(
                            'label' => 'Description',
                            'id' => 'desc',
                            
                            'type' => 'text',
                            
                        ),array(
                            'label' => 'Image',
                            'id' => 'image',
                            
                            'type' => 'upload',
                            
                        )
                    )
            ),array(
                'id' => 'platform_title',
                'label' => 'Platform Title',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            ),array(
                'id' => 'platform_image',
                'label' => 'Platform Image',
                'desc' => '',
                'type' => 'upload',
                'section' => 'init'
            ), array(
                'id' => 'platform_content',
                'label' => 'Platform Content List',
                'desc' => '',
				 'section' => 'init',
                'type' => 'list-item',
                    'class' => '',
                    'settings' => array(
                        array(
                            'label' => 'Description',
                            'id' => 'desc',
                            
                            'type' => 'text',
                            
                        )
                    )
            ),array(
                'id' => 'home_brand_title',
                'label' => 'Home Brand Section Title',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            ),array(
                'id' => 'home_brand_desc',
                'label' => 'Home Brand Description',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            ),array(
                'id' => 'home_brand_image',
                'label' => 'Home Brand Image',
                'desc' => '',
                'type' => 'upload',
                'section' => 'init'
            ),array(
                'id' => 'home_brand_link',
                'label' => 'Home Brand Link',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            ),array(
                'id' => 'home_request_demo_title',
                'label' => 'Home Request Demo Title',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            ),array(
                'id' => 'email',
                'label' => 'Email',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            ),array(
                'id' => 'phone',
                'label' => 'Phone',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            ),array(
                'id' => 'facebook',
                'label' => 'Facebook',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            ),array(
                'id' => 'linkedin',
                'label' => 'Linkedin',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            ),array(
                'id' => 'instragram',
                'label' => 'Instragram',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            ),array(
                'id' => 'youtube',
                'label' => 'Youtube',
                'desc' => '',
                'type' => 'text',
                'section' => 'init'
            )

            
        ),

    );



    if ($saved_settings !== $custom_settings) {
        update_option('option_tree_settings', $custom_settings);
    }
}

?>