<?php
/*
Template Name: Sign In
*/
 get_header();?>
<section class="section section-sign">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-10 col-xl-9 mx-auto">
                    <div class="row">
                        <div class="col-md-6 mx-auto">
                            <div class="sign-case">
                                <div class="sign-body">
                                    <div class="sign-card">
                                        <div class="text-center">
                                            <h1 class="sign-heading">Welcome back!</h1>
                                        </div>
                                        <form id="login" action="login" method="post">
                                            <p class="status"></p>
                                            <div class="form-group">
                                                <input type="text" name="username" class="form-control form-control-lg" id="username" placeholder="Email Address">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="password" class="form-control form-control-lg" id="password" placeholder="Password">
                                            </div>
                                            <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
                                            <div class="form-group text-center">
                                                <button type="submit" class="btn btn-primary w-100 btn-lg btn-log">Log in</button>
                                            </div>
                                            <div class="form-group text-center">
                                                <a target="_blank" href="<?php echo wp_lostpassword_url(); ?>" class="sign-need_help">Need help logging in?</a>
                                            </div>
                                            <div class="f_not-register text-center">
                                                <span class="not-register-title">Not a register yet? </span>
                                                <a href="" class="btn-not-register">Get Started</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <a href="<?php echo home_url();?>" class="sign-go_home">Go to the Kinship homepage</a>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>

 <?php get_footer(); ?>