<?php 
/*
Template Name: Terms 
*/

get_header();?>
<section class="section">
        <div class="container meta-text-widget">
            <h1 class="meta-page-hero-heading">Visitor Registration Terms of Service</h1>
            <p>
                <em>Last updated: June 21, 2019</em>
            </p>
            <p>
                <strong>
                    <em>
                        These terms of use (the “Terms”) and the Privacy Policy govern your access and use of Kinship Pte Ltd.(“Kinship”, “we” or “our”) websites and services offered through our website (the “Services”). Your use of the Services is your agreement to be bound by these Terms and the Privacy Policy. Please read them carefully before using the Services. Your use of the Services is your agreement to be bound by these Terms. Please read them carefully before using the Services. 
                    </em>
                </strong>
            </p>
            <p>
                <strong>
                    <em>
                        If you use the Services on behalf of an entity, company or organization, you agree to these Terms on behalf of such entity, company or organization and promise that you have the authority to bind such entity, company or organization to these Terms. If applicable, “your” will refer to the entity, company or organization that you represent.  
                    </em>
                </strong>
            </p>
            <p>
                <strong>
                    <em>
                        You agree to use the Services in compliance with these Terms. You may use the Services only if you have the authority to form a contract with Kinship and are not barred under any applicable laws from doing so. The Services may change from time to time. These changes and any suspension, or modification of the Services may be made at any time without prior notice to you. We may also remove any content from our Services at our discretion.
                    </em>
                </strong>
            </p>
            <ol class="list-unstyled list-default">
                <li class="text-uppercase">
                    1. Your Files & Your Privacy
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li>
                    1.1 By using our Services you may provide us with information and files that you submit through the Services (collectively, “your Files”). You retain full ownership of your Files. These Terms do not grant us any rights to your Files or intellectual property except for the limited rights that are needed to run the Services, as explained below.
                </li>
                <li>
                    1.2 Your permission may be necessary to do things you ask us to do with your Files, for example, to host or share them with certain individuals within your organization, at your direction. This includes choices we make to administer our Services, for example, how we store and backup your data and Files to keep them safe. You give us the permissions we need to do those things solely to provide the Services. This permission also extends to trusted third parties we work with to provide the Services.
                </li>
                <li>
                    1.3 We will only share your content with others in the rare exceptions we identify in our Privacy Policy, no matter how the Services change. How we collect and use your information generally is also explained in our Privacy Policy.
                </li>
                <li>
                    1.4 You are solely responsible for your conduct, the content of your Files, and your communications with others while using the Services. For example, it’s your responsibility to ensure that you have the rights or permission needed to comply with these Terms.
                </li>
                <li>
                    1.5 You acknowledge that Kinship has no obligation to monitor any information on the Services. We are not responsible for the accuracy, completeness, appropriateness, or legality of files, user posts, or any other information you may be able to access using the Services. We are not responsible for your compliance with applicable employment-related laws and regulations that you may be subject to (including, but not limited to, U.S. Citizenship and Immigration Services, U.S. Department of Labor, and all U.S. States and any international governing bodies). You agree not to retain or store any Form I-9 Employment Eligibility Verification for any employees using the Services. We do not represent to you that we are HIPAA compliant.
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li class="text-uppercase">
                    2. Sharing Your Files
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li>
                    2.1 The Services provide features that allow you to upload your Files to store and share them with certain individuals within your organization. Kinship has no responsibility for that activity.
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li class="text-uppercase">
                    3. Your Responsibilities
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li>
                    3.1 Others may have intellectual property rights to Files and other content in the Services. Please do not copy, upload, download, or share Files unless you have the right to do so. You, not Kinship, will be fully responsible and liable for what you copy, share, upload, download, use and any other actions you take while using the Services. You must not upload spyware or any other malicious software to the Services.
                </li>
                <li>
                    3.2 You, and not Kinship, are responsible for maintaining and protecting all of your Files. K will not be liable for any loss or corruption of your Files, or for any costs or expenses associated with backing up or restoring any of your Files.
                </li>
                <li>
                    3.3 If your contact information, or other information related to your account, changes, you must notify us promptly and keep your information current. The Services are not intended for use by you if you are under 13 years of age. By agreeing to these Terms, you are representing to us that you are over 13 years of age.
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li class="text-uppercase">
                    4. Account Security
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li>
                    4.1 You are responsible for protecting the password that you use to access the Services and you agree not to disclose your password to any third party. You are responsible for any activity using your account, whether or not you authorized that activity. You are also responsible for maintaining and protecting all of your Files other than as specifically stated below. Kinship will not be liable for any loss or corruption of your Files, or for any costs or expenses associated with backing up or restoring any of your Files. You should immediately notify Kinship of any unauthorized use of your account. You acknowledge that if you wish to protect your transmission of data or Files to Kinship, it is your responsibility to use a secure encrypted connection to communicate with the Services.
                </li>
                <li>
                    4.2 In the event of a security breach or loss of your data or Files by (i) your employees or account administrator, (ii) outside parties to whom you may have given access to the Services, or (iii) any other person that gains access as a result of your negligence protecting the security of your account and password, you will maintain responsibility for any resulting damages and agree to take any necessary remedial actions. You will notify Kinship immediately of the breach and steps you expect to take to remedy the breach.
                </li>
                <li>
                    4.3 In the event of a security breach or loss of your data or Files by anyone other than (i) your employees or account administrator, (ii) outside parties to whom you may have given access to the Services, and (iii) any other person that gains access to the Services as a result of your negligence protecting the security of your account, Kinship will immediately notify you and initiate remedial actions that are consistent with industry standards.
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li class="text-uppercase">
                    5. License to Use the Services
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li>
                    5.1 Your license to use the Services is governed by these Terms and also the License Agreement you or your designated account manager has agreed to in creating the applicable account with Kinship (the “License Agreement”). This license is automatically revoked if you violate these Terms in a manner that implicates our intellectual property rights or any other rights. We hereby reserve all rights not expressly granted in these Terms or the License Agreement. To reiterate certain covenants agreed to in the License Agreement, you must not reverse engineer or decompile the Services, nor attempt to do so, nor assist anyone else to do so.
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li class="text-uppercase">
                    6. Kinship Property and Feedback
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li>
                    6.1 These terms do not grant you any right, title, or interest in the Services, or the content in the Services. We may use any feedback, comments, or suggestions you send us without any obligation to you. The software and other technology we use to provide the Services are protected by copyright, trademark, and other laws of both the United States and foreign countries. These Terms do not grant you any rights to use the Kinship trademarks, logos, domain names, or other brand features.
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li class="text-uppercase">
                    7. Acceptable Use Policy
                </li>
            </ol>
            <p>
                <em>
                                In addition to any other things that might constitute a misuse of the Services, you must not, and must not attempt to do the following things:
                            </em>
            </p>
            <ol class="list-unstyled list-default">
                <li>
                    7.1 modify, alter, tamper with, repair or otherwise create derivative works of any of the Services;
                </li>
                <li>
                    7.2 reverse engineer, disassemble or decompile the software used to provide or access the Services, or attempt to discover or recreate the source code used to provide or access the Services, except and only to the extent that the applicable law expressly permits doing so;
                </li>
                <li>
                    7.3 use the Services for research or benchmarking or any related endeavor with the intent of creating a competing or similar product;
                </li>
                <li>
                    7.4 use the Services in any manner or for any purpose other than as expressly permitted by these Terms, the License Agreement, the Privacy Policy, or any other policy, instruction or terms applicable to the Services;
                </li>
                <li>
                    7.5 sell, lend, rent, resell, lease, sublicense or otherwise transfer any of the rights granted to you with respect to the Services to any third party;
                </li>
                <li>
                    7.6 remove, obscure or alter any proprietary rights notice pertaining to the Services;
                </li>
                <li>
                    7.7 access or use the Services in a way intended to improperly avoid incurring fees or exceeding usage limits or quotas;
                </li>
                <li>
                    7.8 use the Services in connection with the operation of nuclear facilities, aircraft navigation, communication systems, medical devices, air traffic control devices, real time control systems or other situations in which the failure of the Services could lead to death, personal injury, or physical property or environmental damage;
                </li>
                <li>
                    7.9 use the Services to: (i) engage in any unlawful or fraudulent activity or perpetrate a hoax or engage in phishing schemes or forgery or other similar falsification or manipulation of data; (ii) send unsolicited or unauthorized junk mail, spam, chain letters, pyramid schemes or any other form of duplicative or unsolicited messages, whether commercial or otherwise; (iii) advertise or promote a commercial product or service that is not available through Kinship; (iv) store or transmit inappropriate content, such as content: (1) containing unlawful, defamatory, threatening, pornographic, abusive, libelous or otherwise objectionable material of any kind or nature, (2) containing any material that encourages conduct that could constitute a criminal offense, or (3) that violates the intellectual property rights or rights to the publicity or privacy of others; (v) store or transmit any content that contains or is used to initiate a denial of service attack, software viruses or other harmful or deleterious computer code, files or programs such as Trojan horses, worms, time bombs, cancelbots, or spyware; or (vi) abuse, harass, stalk or otherwise violate the legal rights of a third party;
                </li>
                <li>
                    7.10 interfere with or disrupt servers or networks used by Kinship to provide the Services or used by other users’ to access the Services, or violate any third party regulations, policies or procedures of such servers or networks or harass or interfere with another user’s full use and enjoyment of any of the Services;
                </li>
                <li>
                    7.11 access or attempt to access Kinship’s other accounts, computer systems or networks not covered by these Terms, through password mining or any other means;
                </li>
                <li>
                    7.12 cause, in Kinship’s sole discretion, inordinate burden on the Services or Kinship’s system resources or capacity; or
                </li>
                <li>
                    7.13 share passwords or other access information or devices or otherwise authorize any third party to access or use the Services.
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li class="text-uppercase">
                    8. Integrations and Third-Party Tools
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li>
                    8.1 Kinship may allow third party integrators (“Integrators“) to create applications and/or tools that supplement or enhance the Services (the “Integrations“). If you choose to access any such Integrations, you (1) agree to abide by these Terms of Use with respect to any such Integrations, and (2) grant permission to the Integrators to access (via API) and use your Files solely for the purpose of supplementing or enhancing the Services through the Integrations. If you are an employee of a Kinship client, you may be requested to use an online document signing tool that is licensed by a third party to Kinship and made available by Kinship to you (“Tool“) to sign documents related to your employment with your employer. Under applicable law, you cannot be forced to sign documents electronically. Therefore, if you object to the use of the Services for those purposes, you should contact your employer directly and request that you be allowed to sign the necessary documents in a physical format. By using the online signature tool, you indicate that you consent to the electronic creation, transmission, and storage of the document. You will have an opportunity to print or save any document formed using the tool, and you should take advantage of that opportunity. Under applicable law, you may also correct any document that you have signed electronically by promptly notifying the party with whom you are contracting of any error or mistake in the document. We suggest you promptly review any document you sign using the Tool to ensure it was correctly done.
                </li>
                <li>
                    8.2 Any warranties related to the Tool will be those made by the third party making the Tool available to Kinship, and Kinship specifically disclaims any express or implied warranties, including without limitation the warranties of fitness for a particular purpose and merchantability, related to use of the Tool. All obligations related to the online execution of documents shall be between you and such third party, and you agree to defend, indemnify, and hold harmless Kinship, its employees and agents, from and against any damages, judgments, settlements, losses, or other liability including without limitation attorneys’ fees, arising out of or related to (1) your and your employer’s use of the Tool, (2) the violation of any requirements related to the formation, conveyance, security, and storage of such documents, and (3) the enforceability of any contract you may form with your employer. You are solely responsible to seek and obtain legal advice relating to the enforceability of your agreements with your employers.
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li class="text-uppercase">
                    9. Copyright
                </li>
            </ol>
            <p>
                <em>
                                Kinship does not tolerate content that appears to infringe any copyright or other intellectual property rights or otherwise violates these Terms and will respond to notices of alleged copyright infringement that comply with the law and are properly provided to us. Such notices can be reported by contacting us at the address below. We reserve the right to delete or disable content alleged to violate these Terms and to terminate repeat infringers. Our contact information for notice of alleged copyright infringement is:
                            </em>
            </p>
            <p>
                <em>
                                <strong>Kinship Pte Ltd</strong><br> 101 Thomson Road <br> 12-01 United Square <br> 307591, Singapore<br> <a href="mailTo:hello@kinship.ai"><span>hello@kinship.ai</span></a>
                            </em>
            </p>
            <ol class="list-unstyled list-default">
                <li class="text-uppercase">
                    10. Termination
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li>
                    10.1 You may stop using our Services any time. We reserve the right to suspend or end the Services at any time, with or without cause, and with or without notice. We may suspend or terminate your use if you are not complying with these Terms, or if you use the Services in any way that would cause us legal liability or disrupt others’ use of the Services. If we suspend or terminate your use, we will attempt to contact you in advance and help you retrieve data, though there may be some cases (for example, repeatedly or flagrantly violating these Terms, a court order, or danger to other users) where we may suspend immediately.
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li class="text-uppercase">
                    11. Kinship Services are Available “AS-IS”
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li>
                    11.1 Kinship PROVIDES THE SERVICES “AS IS”, “WITH ALL FAULTS” AND “AS AVAILABLE”. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, Kinship MAKES NO (AND SPECIFICALLY DISCLAIMS ALL) REPRESENTATIONS OR WARRANTIES OF ANY KIND, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, ANY WARRANTY THAT THE SERVICES WILL BE UNINTERRUPTED, ERROR-FREE OR FREE OF HARMFUL COMPONENTS, THAT THE CONTENT WILL BE SECURE OR NOT OTHERWISE LOST OR DAMAGED, OR ANY IMPLIED WARRANTY OF MERCHANTABILITY, SATISFACTORY QUALITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, AND ANY WARRANTY ARISING OUT OF ANY COURSE OF PERFORMANCE, COURSE OF DEALING OR USAGE OF TRADE. SOME JURISDICTIONS DO NOT ALLOW THE FOREGOING DISCLAIMERS. IN SUCH AN EVENT SUCH DISCLAIMER WILL NOT APPLY EXCEPT TO THE EXTENT ALLOWED BY APPLICABLE LAW.
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li class="text-uppercase">
                    12. Limitation of Liability
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li>
                    12.1 TO THE FULLEST EXTENT PERMITTED BY LAW, IN NO EVENT WILL Kinship, ITS AFFILIATES, SHAREHOLDERS, MEMBERS, OFFICERS, EMPLOYEES, AGENTS, SUPPLIERS OR LICENSORS BE LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE, COVER OR CONSEQUENTIAL DAMAGES (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOST PROFITS, REVENUE, GOODWILL, USE OR CONTENT) HOWEVER CAUSED, UNDER ANY THEORY OF LIABILITY, INCLUDING, WITHOUT LIMITATION, CONTRACT, TORT, WARRANTY, NEGLIGENCE OR OTHERWISE, EVEN IF Kinship HAS BEEN ADVISED AS TO THE POSSIBILITY OF SUCH DAMAGES. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THE AGGREGATE LIABILITY OF Kinship AND ITS AFFILIATES, SHAREHOLDERS, MEMBERS, OFFICERS, EMPLOYEES, AGENTS, SUPPLIERS OR LICENSORS, RELATING TO THE SERVICES WILL BE LIMITED TO THE TOTAL AMOUNT PAID BY YOU TO Kinship DURING THE IMMEDIATELY PRECEDING TWELVE MONTH PERIOD (DETERMINED AS OF THE DATE OF ANY FINAL JUDGMENT IN AN ACTION) OR FIVE DOLLARS ($5.00), WHICHEVER IS GREATER. THE LIMITATIONS AND EXCLUSIONS ALSO APPLY IF THIS REMEDY DOES NOT FULLY COMPENSATE YOU FOR ANY LOSSES OR FAILS OF ITS ESSENTIAL PURPOSE. SOME JURISDICTIONS DO NOT ALLOW THE LIMITATION OF INCIDENTAL, CONSEQUENTIAL OR OTHER DAMAGES. IN SUCH AN EVENT THIS LIMITATION WILL NOT APPLY TO YOU TO THE EXTENT PROHIBITED BY LAW.
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li class="text-uppercase">
                    13. Modifications
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li>
                    13.1 We reserve the right to revise these Terms from time to time. We will date and post the most current version of these Terms on our website. Any changes will be effective upon posting the revised version of these Terms on the site (or such later effective date as may be indicated at the top of the revised Terms). If in our sole discretion we deem a revision to these Terms to be material, we will notify you via the Services and/or by email to the email address associated with your account. Therefore, we encourage you to check the date of these Terms whenever you visit our site to see if these Terms have been updated. Your continued access or use of any portion of the Services constitutes your acceptance of such changes. If you disagree with any of the changes, we are not obligated to continue providing the Services to you, and you must cancel and stop using the Services.
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li class="text-uppercase">
                    14. Miscellaneous Legal Terms
                </li>
            </ol>
            <ol class="list-unstyled list-default">
                <li>
                    14.1 THESE TERMS AND THE USE OF THE SERVICES WILL BE GOVERNED BY ILLINOIS LAW EXCEPT FOR ITS CONFLICTS OF LAWS PRINCIPLES. ALL CLAIMS ARISING OUT OF OR RELATING TO THESE TERMS OR THE SERVICES MUST BE LITIGATED EXCLUSIVELY IN THE FEDERAL OR STATE COURTS OF COOK COUNTY, ILLINOIS, AND BOTH PARTIES CONSENT TO VENUE AND PERSONAL JURISDICTION THERE. These Terms, the Privacy Policy and License Agreement together constitute the entire and exclusive agreement between you and Kinship with respect to the Services, and supersede and replace any other agreements, terms and conditions applicable to the Services. These Terms create no third party beneficiary rights. The failure of either party to insist upon or enforce strict performance of any of the provisions of these Terms or to exercise any rights or remedies under these Terms will not be construed as a waiver or relinquishment to any extent of such party’s right to assert or rely upon any such provision, right or remedy in that or any other instance; rather, the same will remain in full force and effect. If a provision is found unenforceable the remaining provisions of the Agreement will remain in full effect and an enforceable term will be substituted reflecting our intent as closely as possible. We may assign, transfer, or otherwise dispose of our rights and obligations under these Terms, in whole or in part, at any time without notice. You may not assign or transfer any rights to use the Services. Kinship and you are not legal partners or agents; instead, our relationship is that of independent contractors.
                </li>
            </ol>
        </div>
    </section>
<?php get_footer();?>