<?php
/*
Template Name: solutions
*/
 get_header();?>


<section class="section hero-pages-secondary d-flex align-items-center">
        <div class="container relative">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h1 class="hero-heading">
                        <?php the_title();?>
                    </h1>
                    <div class="hero-text">
                        <p>
                            <?php
                            if (have_posts()) {
                                while (have_posts()) {
                                    the_post();
                                    the_content();
                                }
                            } ?>
                        </p>
                    </div>
                </div>
                <div class="col-md-5 offset-md-1">
                    <?php if (has_post_thumbnail(get_the_ID())): $image = get_the_post_thumbnail_url(get_the_ID(), 'full') ?>
                    <img src="<?php echo $image;?>" alt="" class="img-fluid">
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php
$allspecial=get_post_meta(get_the_ID(),'special_ins',true);
$features =get_post_meta(get_the_ID(),'features',true);
?>
<?php if(array_key_exists(0, $features)): ?>
    <section class="section">
        <div class="container">
            <div class="row manufac__flexbar manufac__flexbar-image-right">
                <div class="col-md-6 offset-md-1 order-md-2 manufac-nag__image-column">
                    <div class="manufac-nag-image-box">
                        <div class="manufac-nag-image">

                            <img src="<?php echo $features[0]['image'];?>" alt="...">

                        </div>
                    </div>
                </div>
                <div class="col-md-5 align-self-center">
                    <h2 class="manufac__flexbar-title"><?php echo $features[0]['title'];?></h2>
                    <div class="manufac__flexbar-text">
                        <p>
                            <?php echo $features[0]['desc'];?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

    <section class="section pt-0">
        <div class="container">
            <div class="row">
            <?php foreach ($allspecial as $alls): ?>
                <div class="col-md-4 c-mb">
                    <div class="net-card card__blank text-left">
                        <div class="nc-icon">
                            <img src="<?php echo $alls['image']; ?>" alt="...">
                        </div>
                        <div class="nc-denote">
                            <h5 class="nc-title"><?php echo $alls['title']; ?></h5>
                            <div class="nc-text">
                                <?php echo $alls['desc']; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </section>


    <section id="customer_story" class="section section-split-bg split-bg-w_80 d-flex align-items-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12 col-lg-5 offset-lg-1 order-lg-2">
                    <div class="perspective-image">
                        <img src="<?php echo get_template_directory_uri();?>/assets/images/demo/2.jpg" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <h6>CUSTOMER STORY</h6>
                    <h2>Making employee satisfaction the top priority at Autobutler</h2>
                    <p>
                        Autobutler was created to improve transparency in the automotive industry, yet prior to Peakon, transparency around employee engagement had not been an area of focus for the business. Co-founder, Peter Zigler explains "Peakon has literally changed how we're working with employee satisfaction at Autobutler. It has given us the ability to be much more detailed and sophisticated in our decision making
                    </p>

                    <h5 class="mt-5 mb-3">Results Achieved</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="achieved-item">
                                <div class="achieved-icon">
                                    <i class="far fa-check-circle"></i>
                                </div>
                                <p>
                                    <strong>Gathered</strong> employee feedback across remote locations worldwide
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="achieved-item">
                                <div class="achieved-icon">
                                    <i class="far fa-check-circle"></i>
                                </div>
                                <p>
                                    <strong>Identified</strong> strengths and priorities for teams, regions and the organisation as a whole
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="achieved-item">
                                <div class="achieved-icon">
                                    <i class="far fa-check-circle"></i>
                                </div>
                                <p>
                                    <strong>Provided</strong> real-time insights to managers throughout the business
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="achieved-item">
                                <div class="achieved-icon">
                                    <i class="far fa-check-circle"></i>
                                </div>
                                <p>
                                    <strong>Built</strong> a successful and unified company culture across multiple locations
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-6 mx-auto text-center">
                    <h2 class="color-primary mb-4">The many ways Peakon can support your organisation</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 c-mb">
                    <div class="organisation-item">
                        <div>
                            <div class="organisation-icon">
                                <img src="<?php echo get_template_directory_uri();?>/assets/images/icons/Dashboard.svg" alt="">
                            </div>   
                        </div>
                        <div class="organisation-denote">
                            <h5>Employee Engagement</h5>
                            <div class="organisation-text">
                               Increase productivity, retention and business performance with Peakon 
                            </div>
                            <a href="" class="text-link">Learn more</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 c-mb">
                    <div class="organisation-item">
                        <div>
                            <div class="organisation-icon">
                                <img src="<?php echo get_template_directory_uri();?>/assets/images/icons/Training.svg" alt="">
                            </div>   
                        </div>
                        <div class="organisation-denote">
                            <h5>Employee Engagement</h5>
                            <div class="organisation-text">
                               Increase productivity, retention and business performance with Peakon 
                            </div>
                            <a href="" class="text-link">Learn more</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 c-mb">
                    <div class="organisation-item">
                        <div>
                            <div class="organisation-icon">
                                <img src="<?php echo get_template_directory_uri();?>/assets/images/icons/Analytics.svg" alt="">
                            </div>   
                        </div>
                        <div class="organisation-denote">
                            <h5>Employee Engagement</h5>
                            <div class="organisation-text">
                               Increase productivity, retention and business performance with Peakon 
                            </div>
                            <a href="" class="text-link">Learn more</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 c-mb">
                    <div class="organisation-item">
                        <div>
                            <div class="organisation-icon">
                                <img src="<?php echo get_template_directory_uri();?>/assets/images/icons/Analytics.svg" alt="">
                            </div>   
                        </div>
                        <div class="organisation-denote">
                            <h5>Employee Engagement</h5>
                            <div class="organisation-text">
                               Increase productivity, retention and business performance with Peakon 
                            </div>
                            <a href="" class="text-link">Learn more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="section_globe" class="section section-split-bg split-bg-w_55 d-flex align-items-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <h2>Powering Manufacturing & Construction businesses around the globe</h2>
                </div>
                <div class="col-md-8">
                    <div class="industry__logos-container">
                        <div class="industry__logo">
                            <img src="<?php echo get_template_directory_uri();?>/assets/images/demo/bovis@2x.webp" alt="">
                        </div>
                        <div class="industry__logo">
                            <img src="<?php echo get_template_directory_uri();?>/assets/images/demo/bovis@2x.webp" alt="">
                        </div>
                        <div class="industry__logo">
                            <img src="<?php echo get_template_directory_uri();?>/assets/images/demo/bovis@2x.webp" alt="">
                        </div>
                        <div class="industry__logo">
                            <img src="<?php echo get_template_directory_uri();?>/assets/images/demo/bovis@2x.webp" alt="">
                        </div>
                        <div class="industry__logo">
                            <img src="<?php echo get_template_directory_uri();?>/assets/images/demo/bovis@2x.webp" alt="">
                        </div>
                        <div class="industry__logo">
                            <img src="<?php echo get_template_directory_uri();?>/assets/images/demo/bovis@2x.webp" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    

<?php get_footer();?>