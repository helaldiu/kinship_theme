<?php get_header();?>
<section class="section">
	<?php if(have_posts()) : ?>
					<?php while (have_posts()) : the_post();?>
        <div class="container meta-text-widget">
            <h1 class="meta-page-hero-heading"> <?php the_title();?></h1>
<?php the_content(); ?>
            </div>
            <?php endwhile; ?><?php endif; ?>

  <?php wp_reset_query();?>
        </section>
<?php get_footer();?>