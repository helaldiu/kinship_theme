<?php
add_action('init', 'kinship_members');

function kinship_members() {
    $labels = array(
        'name' => _x('Member', 'post type general name', 'kinship'),
        'singular_name' => _x('Member', 'post type singular name', 'kinship'),
        'menu_name' => _x('Leadership', 'admin menu', 'kinship'),
        'name_admin_bar' => _x('Member', 'add new on admin bar', 'kinship'),
        'add_new' => _x('Add New', 'Member', 'kinship'),
        'add_new_item' => __('Add New Member', 'kinship'),
        'new_item' => __('New Member', 'kinship'),
        'edit_item' => __('Edit Member', 'kinship'),
        'view_item' => __('View Member', 'kinship'),
        'all_items' => __('All Member', 'kinship'),
        'search_items' => __('Search Member', 'kinship'),
        'parent_item_colon' => __('Parent Member:', 'kinship'),
        'not_found' => __('No Member found.', 'kinship'),
        'not_found_in_trash' => __('No Member found in Trash.', 'kinship')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'kinship-leaders'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'thumbnail'),
        'menu_icon' => 'dashicons-businessman'
    );

    register_post_type('kinship-leaders', $args);


}

function kinmem_change_title_text( $title ){
    $screen = get_current_screen();

    if  ( 'kinship-leaders' == $screen->post_type ) {
        $title = 'Enter Member name';
    }

    return $title;
}

add_filter( 'enter_title_here', 'kinmem_change_title_text' );




function ST4_get_featured_image($post_ID) {
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);
    if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'small');
        return $post_thumbnail_img[0];
    }
}

// ADD NEW COLUMN
function ST4_columns_head($defaults) {
    $defaults['featured_image'] = 'Member Image';
    return $defaults;
}

// SHOW THE FEATURED IMAGE
function ST4_columns_content($column_name, $post_ID) {
    if ($column_name == 'featured_image') {
        $post_featured_image = ST4_get_featured_image($post_ID);
        if ($post_featured_image) {
            echo '<img style="width:98px; height:98px; border-radius: 50%; border: 2px solid #548eeb;" src="' . $post_featured_image . '" />';
        } else {
            // NO FEATURED IMAGE, SHOW THE DEFAULT ONE
            echo '<img style="width:100px; height:100px;" src="' . get_bloginfo('template_url') . '/portfolios/images/default.svg" />';
        }
    }
}
add_filter('manage_kinship-leaders_posts_columns', 'ST4_columns_head');
add_action('manage_kinship-leaders_posts_custom_column', 'ST4_columns_content', 5, 2);

add_filter('manage_kinship-leaders_posts_columns', 'kin_member_table_head');
add_action( 'manage_kinship-leaders_posts_custom_column', 'kin_member_table_content', 6, 3 );
function kin_member_table_head( $columns ) {
    $columns['member_role']  = 'Position';
    return $columns;

}


function kin_member_table_content( $column_name, $post_id ) {
    if( $column_name == 'member_role' ) {
        $featured_product = get_post_meta( $post_id, 'designation', true );
        if(!empty($featured_product)) {
            echo '<i>'.$featured_product.'</i>';
        }
    }
}

function kin_member_reorder_columns($columns) {
    $kin_member_columns = array();
    $title = 'title';
    foreach($columns as $key => $value) {
        if ($key==$title){
            $kin_member_columns['featured_image'] = '';
            $kin_member_columns['title'] = '';
            $kin_member_columns['member_role'] = '';
        }
        $kin_member_columns[$key] = $value;
    }
    return $kin_member_columns;
}
add_filter('manage_kinship-leaders_posts_columns', 'kin_member_reorder_columns');



