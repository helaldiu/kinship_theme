<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package kinship
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function kinship_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'kinship_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function kinship_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'kinship_pingback_header' );


add_action( 'wp_ajax_custom_action', 'custom_action' );
add_action( 'wp_ajax_nopriv_custom_action', 'custom_action' );
function custom_action() {
    $response = array('msg' => "", 'error' => "");
    if ( 
        ! isset( $_POST['_rd_nonce'] ) 
        || ! wp_verify_nonce( $_POST['_rd_nonce'], 'custom_action_nonce') 
    ) {
 
        exit('The form is not valid');
 
    } else {
        $f_name = $_POST['first_name'];
        $l_name = $_POST['last_name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $org = $_POST['org'];
        $country = $_POST['country'];

        if(!isset($f_name) || empty($f_name)) { $response['error'] .= "<li>First Name Required</li>"; }
        if(!isset($l_name) || empty($l_name)) { $response['error'] .= "<li>Last Name Required</li>"; }
        if(!is_email($email)) { $response['error'] .= "<li>Email Not Valid</li>"; }
        if(!isset($phone) || empty($phone)) { $response['error'] .= "<li>Phone Number Required</li>"; }
        if(!isset($org) || empty($org)) { $response['error'] .= "<li>Organization/Business name Required</li>"; }
        if(!isset($country) || empty($country)) { $response['error'] .= "<li>Please Select Country</li>"; }

        if(empty($response['error'])){
            $to = 'kutsnalmas@gmail.com';//get_bloginfo('admin_email');
            $subject = 'Request Demo';
            $body = '<h5>Demo Request from:</h5> <p><b>Name : </b> '.$f_name.' '.$l_name.' </p> <p><b>Email : </b> '.$email.' </p> <p><b>Phone : </b> '.$phone.' </p><p><b>Org/Business : </b> '.$org.' </p><p><b>Country : </b> '.$country.' </p>';
            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Kinship <info@kinship.io>');
            if(wp_mail( $to, $subject, $body, $headers )){

            } else {
                mail($to, $subject, $body, 'Content-Type: text/html; charset=UTF-8'."\r\n".'From: Kinship <info@kinship.io>'."\r\n");
            }
            $response['msg'] = "<li>Success!!</li>";
        }

    	wp_send_json($response);
    }
 
    // ... Processing further
}
