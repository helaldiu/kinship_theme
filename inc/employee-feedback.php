<?php
add_action('init', 'kinship_emp_feed');

function kinship_emp_feed() {
    $labels = array(
        'name' => _x('Feedback', 'post type general name', 'kinship'),
        'singular_name' => _x('Feedback', 'post type singular name', 'kinship'),
        'menu_name' => _x('Employee Feedback', 'admin menu', 'kinship'),
        'name_admin_bar' => _x('Feedback', 'add new on admin bar', 'kinship'),
        'add_new' => _x('Add New', 'Feedback', 'kinship'),
        'add_new_item' => __('Add New Feedback', 'kinship'),
        'new_item' => __('New Feedback', 'kinship'),
        'edit_item' => __('Edit Feedback', 'kinship'),
        'view_item' => __('View Feedback', 'kinship'),
        'all_items' => __('All Feedback', 'kinship'),
        'search_items' => __('Search Feedback', 'kinship'),
        'parent_item_colon' => __('Parent Feedback:', 'kinship'),
        'not_found' => __('No Feedback found.', 'kinship'),
        'not_found_in_trash' => __('No Feedback found in Trash.', 'kinship')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'employee-feedback'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'thumbnail'),
        'menu_icon' => 'dashicons-format-status'
    );

    register_post_type('employee-feedback', $args);


}

