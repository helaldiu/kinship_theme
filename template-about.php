<?php
/*
Template Name: About
*/
get_header();
?>
<section class="section hero-pages-default d-flex align-items-center">
        <div class="container relative">
            <div class="row">
                <div class="col-md-11 col-lg-8 mx-auto text-center">
                    <h1 class="hero-heading">
                        <?php the_title();?>
                    </h1>
                </div>
                <div class="col-md-12 col-lg-7 mx-auto text-center">
                    <div class="hero-text">
                        <p>
                            <?php
                            if (have_posts()) {
                            while (have_posts()) {
                            the_post();
                            the_content();
                            }
                            } ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
$allspecial=get_post_meta(get_the_ID(),'special_ins',true);
$features =get_post_meta(get_the_ID(),'features',true);
?>

    <section class="bg-light-gredient">
		<div class="section section-about-empty block-py-xl d-flex align-items-center">
			<div class="container">
				<div class="row">
                    <div class="col-md-6">
                        <div class="about_highlight-para">
                          
                            <p>
                                <?php  
                                $about_content=get_post_meta(get_the_ID(),'subtitle',true);
                                echo $about_content;
                                ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="about_highlight-para">
                            <p>
                                <?php  $about_content_extra=get_post_meta(get_the_ID(),'subtitle_extra',true);
                                    echo $about_content_extra;
                                ?>
                            </p>
                            <h2 class="about_highlight-title text-secondary mt-4">
                            <?php   $about_content_title=get_post_meta(get_the_ID(),'about_extra_title',true);
                                echo $about_content_title;
                            ?>
                            <span class="text-primary"><?php  $about_content_title=get_post_meta(get_the_ID(),'about_extra_title_red',true);
                                echo $about_content_title; ?></span> in the supply chain.
                            </h2>
                        </div>
                    </div>
                </div>
			</div>
		</div>
		<!--
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="pr-md-3 pr-lg-5 about_highlight-title t-primary">
                        <?php if(array_key_exists(0, $allspecial)): echo $allspecial[0]['desc']; endif; ?>
                    </h4>
                </div>
                <div class="col-md-6">
                    <div class="about_highlight-para">
                        <?php if(array_key_exists(1, $allspecial)): echo $allspecial[1]['desc']; endif; ?>
                    </div>
                </div>
            </div>
        </div>
		-->
		<div class="section-company-growth">
            <div class="company-growth-chart">
                <div class="growth-charts">
                    <div class="growth-charts-chart">
                        <ul class="growth-charts-labels">
                            <li class="growth-label growth-label--one">
                                <div class="growth-label-content">
                                    <div class="growth-label-entry">
                                        <div class="growth-label-headline">
                                            <span class="growth-label-icon">
                                                <svg width="32" height="32" viewBox="0 0 32 32">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <path class="path1" fill="#333E63" fill-rule="nonzero" d="M17 26h-2v-2h-4v2H7a3 3 0 0 1-3-3V9a3 3 0 0 1 3-3h18a3 3 0 0 1 3 3v14a3 3 0 0 1-3 3h-4v-2h-4v2zM7 8a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h18a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1H7z"></path>
                                                        <path class="path2" fill="#54D4F2" d="M15 19h2v9h-2z"></path>
                                                        <path class="stroke path3" stroke="#54D4F2" stroke-width="2" d="M12.464 21.464L16 17.93l3.536 3.535"></path>
                                                        <path class="path4" fill="#333E63" d="M4 11h24v2H4z"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            <span class="growth-label-year">2017</span>
                                        </div>
                                        <div class="growth-label-text">
                                            Kutumbita (previously known as) was formed and started a pilot in our first factory.
                                        </div>
                                    </div>
                                    <div class="growth-label-line"></div>
                                </div>
                            </li>
                            <li class="growth-label growth-label--two">
                                <div class="growth-label-content">
                                    <div class="growth-label-entry">
                                        <div class="growth-label-headline">
                                            <span class="growth-label-icon">
                                                <svg width="32" height="32" viewBox="0 0 32 32">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <path class="path1" fill="#333E63" fill-rule="nonzero" d="M17 26h-2v-2h-4v2H7a3 3 0 0 1-3-3V9a3 3 0 0 1 3-3h18a3 3 0 0 1 3 3v14a3 3 0 0 1-3 3h-4v-2h-4v2zM7 8a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h18a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1H7z"></path>
                                                        <path class="path2" fill="#54D4F2" d="M15 19h2v9h-2z"></path>
                                                        <path class="stroke path3" stroke="#54D4F2" stroke-width="2" d="M12.464 21.464L16 17.93l3.536 3.535"></path>
                                                        <path class="path4" fill="#333E63" d="M4 11h24v2H4z"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            <span class="growth-label-year">2017</span>
                                        </div>
                                        <div class="growth-label-text">
                                            After a successful pilot, we were covered on various reputed news outlets for creating a mobile app for factory workers.
                                        </div>
                                    </div>
                                    <div class="growth-label-line"></div>
                                </div>
                            </li>
                            <li class="growth-label growth-label--three">
                                <div class="growth-label-content">
                                    <div class="growth-label-entry">
                                        <div class="growth-label-headline">
                                            <span class="growth-label-icon">
                                                <svg width="32" height="32" viewBox="0 0 32 32">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <path class="path1" fill="#333E63" fill-rule="nonzero" d="M17 26h-2v-2h-4v2H7a3 3 0 0 1-3-3V9a3 3 0 0 1 3-3h18a3 3 0 0 1 3 3v14a3 3 0 0 1-3 3h-4v-2h-4v2zM7 8a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h18a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1H7z"></path>
                                                        <path class="path2" fill="#54D4F2" d="M15 19h2v9h-2z"></path>
                                                        <path class="stroke path3" stroke="#54D4F2" stroke-width="2" d="M12.464 21.464L16 17.93l3.536 3.535"></path>
                                                        <path class="path4" fill="#333E63" d="M4 11h24v2H4z"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            <span class="growth-label-year">2018</span>
                                        </div>
                                        <div class="growth-label-text">
                                            Accepted to globally renowned Techstars Impact Accelerator in Austin, Texas.
                                        </div>
                                    </div>
                                    <div class="growth-label-line"></div>
                                </div>
                            </li>
                            <li class="growth-label growth-label--four">
                                <div class="growth-label-content">
                                    <div class="growth-label-entry">
                                        <div class="growth-label-headline">
                                            <span class="growth-label-icon">
                                                <svg width="32" height="32" viewBox="0 0 32 32">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <path class="path1" fill="#333E63" fill-rule="nonzero" d="M17 26h-2v-2h-4v2H7a3 3 0 0 1-3-3V9a3 3 0 0 1 3-3h18a3 3 0 0 1 3 3v14a3 3 0 0 1-3 3h-4v-2h-4v2zM7 8a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h18a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1H7z"></path>
                                                        <path class="path2" fill="#54D4F2" d="M15 19h2v9h-2z"></path>
                                                        <path class="stroke path3" stroke="#54D4F2" stroke-width="2" d="M12.464 21.464L16 17.93l3.536 3.535"></path>
                                                        <path class="path4" fill="#333E63" d="M4 11h24v2H4z"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            <span class="growth-label-year">2018</span>
                                        </div>
                                        <div class="growth-label-text">
                                            After 3 months of deep-diving with world class mentors and support from Techstars we embarked onto building our latest version, an AI powered SaaS platform for factories.
                                        </div>
                                    </div>
                                    <div class="growth-label-line"></div>
                                </div>
                            </li>
                            <li class="growth-label growth-label--five">
                                <div class="growth-label-content">
                                    <div class="growth-label-entry">
                                        <div class="growth-label-headline">
                                            <span class="growth-label-icon">
                                                <svg width="32" height="32" viewBox="0 0 32 32">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <path class="path1" fill="#333E63" fill-rule="nonzero" d="M17 26h-2v-2h-4v2H7a3 3 0 0 1-3-3V9a3 3 0 0 1 3-3h18a3 3 0 0 1 3 3v14a3 3 0 0 1-3 3h-4v-2h-4v2zM7 8a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h18a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1H7z"></path>
                                                        <path class="path2" fill="#54D4F2" d="M15 19h2v9h-2z"></path>
                                                        <path class="stroke path3" stroke="#54D4F2" stroke-width="2" d="M12.464 21.464L16 17.93l3.536 3.535"></path>
                                                        <path class="path4" fill="#333E63" d="M4 11h24v2H4z"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            <span class="growth-label-year">2019</span>
                                        </div>
                                        <div class="growth-label-text">
                                            Received grant from Humanity United of Omidyar Network and expanded our core development team.
                                        </div>
                                    </div>
                                    <div class="growth-label-line"></div>
                                </div>
                            </li>
                            <li class="growth-label growth-label--six">
                                <div class="growth-label-content">
                                    <div class="growth-label-entry">
                                        <div class="growth-label-headline">
                                            <span class="growth-label-icon">
                                                <svg width="32" height="32" viewBox="0 0 32 32">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <path class="path1" fill="#333E63" fill-rule="nonzero" d="M17 26h-2v-2h-4v2H7a3 3 0 0 1-3-3V9a3 3 0 0 1 3-3h18a3 3 0 0 1 3 3v14a3 3 0 0 1-3 3h-4v-2h-4v2zM7 8a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h18a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1H7z"></path>
                                                        <path class="path2" fill="#54D4F2" d="M15 19h2v9h-2z"></path>
                                                        <path class="stroke path3" stroke="#54D4F2" stroke-width="2" d="M12.464 21.464L16 17.93l3.536 3.535"></path>
                                                        <path class="path4" fill="#333E63" d="M4 11h24v2H4z"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            <span class="growth-label-year">2019</span>
                                        </div>
                                        <div class="growth-label-text">
                                            Launched new platform, rebranded to Kinship AI, added new customers and onboarded almost 30,000+ workers.
                                        </div>
                                    </div>
                                    <div class="growth-label-line"></div>
                                </div>
                            </li>
                            <li class="growth-label growth-label--seven">
                                <div class="growth-label-content">
                                    <div class="growth-label-entry">
                                        <div class="growth-label-headline">
                                            <span class="growth-label-icon">
                                                <svg width="32" height="32" viewBox="0 0 32 32">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <path class="path1" fill="#333E63" fill-rule="nonzero" d="M17 26h-2v-2h-4v2H7a3 3 0 0 1-3-3V9a3 3 0 0 1 3-3h18a3 3 0 0 1 3 3v14a3 3 0 0 1-3 3h-4v-2h-4v2zM7 8a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h18a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1H7z"></path>
                                                        <path class="path2" fill="#54D4F2" d="M15 19h2v9h-2z"></path>
                                                        <path class="stroke path3" stroke="#54D4F2" stroke-width="2" d="M12.464 21.464L16 17.93l3.536 3.535"></path>
                                                        <path class="path4" fill="#333E63" d="M4 11h24v2H4z"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            <span class="growth-label-year">2020</span>
                                        </div>
                                        <div class="growth-label-text">
                                            New features in Q1:  digital pay (salaries), insurance on mobile and face detection enabled kiosks. 
                                        </div>
                                    </div>
                                    <div class="growth-label-line"></div>
                                </div>
                            </li>
                            <li class="growth-label growth-label--eight">
                                <div class="growth-label-content">
                                    <div class="growth-label-entry">
                                        <div class="growth-label-headline">
                                            <span class="growth-label-icon">
                                                <svg width="32" height="32" viewBox="0 0 32 32">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <path class="path1" fill="#333E63" fill-rule="nonzero" d="M17 26h-2v-2h-4v2H7a3 3 0 0 1-3-3V9a3 3 0 0 1 3-3h18a3 3 0 0 1 3 3v14a3 3 0 0 1-3 3h-4v-2h-4v2zM7 8a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h18a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1H7z"></path>
                                                        <path class="path2" fill="#54D4F2" d="M15 19h2v9h-2z"></path>
                                                        <path class="stroke path3" stroke="#54D4F2" stroke-width="2" d="M12.464 21.464L16 17.93l3.536 3.535"></path>
                                                        <path class="path4" fill="#333E63" d="M4 11h24v2H4z"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                            <span class="growth-label-year">2020</span>
                                        </div>
                                        <div class="growth-label-text">
                                            Expansion planned for South-East Asia and more verticals in manufacturing.
                                        </div>
                                    </div>
                                    <div class="growth-label-line"></div>
                                </div>
                            </li>
                        </ul>
                        <div class="growth-chart-mobile-dot"></div>
                        <img src="<?php echo get_template_directory_uri();?>/assets/images/growth.png" alt="" class="growth-chart-image">
                        <div class="growth-extra-bg"></div>
                    </div>
                </div>
            </div> 
        </div>
    </section>

    <section class="section pt-0 d-none">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="section-heading color-primary">Leadership</h2>
                </div>
            </div>
            <div class="row">
                <?php
                    $blog = new WP_Query(array('post_type' => 'kinship-leaders', 'posts_per_page' => 6, 'order' => 'asc'));
                    if ($blog->have_posts()) :
                        while ($blog->have_posts()) :
                            $blog->the_post();
                            $image = get_the_post_thumbnail_url(get_the_ID(), 'medium');
                            $designation = get_post_meta( get_the_ID(), 'designation', true );
                            $twitter = get_post_meta( get_the_ID(), 'twitter-url', true );
                            $linkedin = get_post_meta( get_the_ID(), 'linkedin-url', true );
                ?>
                <div class="col-md-4 col-team">
                    <div class="team-item">
                        <div class="team-avater">
                            <img src="<?php echo $image; ?>" alt="...">
                        </div>
                        <div class="team-denote">
                            <h5 class="team-name"><?php echo get_the_title(); ?></h5>
                            <div class="team-designation"><?php if(metadata_exists('post', get_the_ID(), 'designation')): echo $designation; endif; ?></div>
                            <ul class="team-socials">
                                <?php if(metadata_exists('post', get_the_ID(), 'twitter-url')): ?>
                                <li>
                                    <a target="_blank" href="<?php echo $twitter; ?>"><i class="fab fa-twitter"></i></a>
                                </li>
                                <?php
                                    endif;
                                    if(metadata_exists('post', get_the_ID(), 'linkedin-url')):
                                ?>
                                <li>
                                    <a href="<?php echo $linkedin; ?>"><i class="fab fa-linkedin-in"></i></a>
                                </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php
                        endwhile;
                    endif;

                ?>

            </div>
        </div>
    </section>

	<section class="section section-team">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-10 mx-auto">
                    <ul class="nav nav-team justify-content-center" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="team-tab" data-toggle="tab" href="#team" role="tab" aria-controls="team" aria-selected="true">Team</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="advisors-tab" data-toggle="tab" href="#advisors" role="tab" aria-controls="advisors" aria-selected="false">Advisors</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-12 col-lg-10 mx-auto team-tab-content-height">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="team" role="tabpanel" aria-labelledby="team-tab">
                            <div class="row justify-content-center">
                                <div class="col-md-4 col-team">
                                    <div class="team-item">
                                        <div class="team-avater ajax-popup">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/images/team/rameez-hoque.jpg" alt="...">
                                        </div>
                                        <div class="team-denote">
                                            <h5 class="team-name">Rameez Hoque</h5>
                                            <div class="team-designation">CEO & Co-founder</div>
                                            <ul class="team-socials">
                                                <li>
                                                    <a href="https://www.linkedin.com/in/rameezhoque/" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-team">
                                    <div class="team-item">
                                        <div class="team-avater ajax-popup">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/images/team/boutaina-faruq.jpg" alt="...">
                                        </div>
                                        <div class="team-denote">
                                            <h5 class="team-name">Boutaina Faruq</h5>
                                            <div class="team-designation">Co-founder</div>
                                            <ul class="team-socials">
                                                <li>
                                                    <a href="https://www.linkedin.com/in/boutaina-faruq-a725a4162/" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-team">
                                    <div class="team-item">
                                        <div class="team-avater ajax-popup">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/images/team/shafkat.jpg" alt="...">
                                        </div>
                                        <div class="team-denote">
                                            <h5 class="team-name">Shafkat Alam</h5>
                                            <div class="team-designation">CTO</div>
                                            <ul class="team-socials">
                                                <li>
                                                    <a href="https://www.linkedin.com/in/shafkatalam/" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-team">
                                    <div class="team-item">
                                        <div class="team-avater ajax-popup">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/images/team/richie-kotwal.jpg" alt="...">
                                        </div>
                                        <div class="team-denote">
                                            <h5 class="team-name">Richie Kotwal</h5>
                                            <div class="team-designation">Head of Business Development</div>
                                            <ul class="team-socials">
                                                <li>
                                                    <a href="https://www.linkedin.com/in/richie-kotwal-08b11558/" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-team">
                                    <div class="team-item">
                                        <div class="team-avater ajax-popup">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/images/team/alvi.jpg" alt="...">
                                        </div>
                                        <div class="team-denote">
                                            <h5 class="team-name">Alvi Awwal</h5>
                                            <div class="team-designation">Head of Operations, South Asia</div>
                                            <ul class="team-socials">
                                                <li>
                                                    <a href="https://www.linkedin.com/in/alvi-awwal-47887848/" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="advisors" role="tabpanel" aria-labelledby="advisors-tab">
                            <div class="row justify-content-center">
                                <div class="col-md-4 col-team">
                                    <div class="team-item">
                                        <div class="team-avater ajax-popup">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/images/team/zoe-schlag.jpg" alt="...">
                                        </div>
                                        <div class="team-denote">
                                            <h5 class="team-name">Zoe Schlag</h5>
                                            <div class="team-designation">Managing Director, Techstars Impact</div>
                                            <ul class="team-socials">
                                                <li>
                                                    <a href="javascript:void()"><i class="fab fa-linkedin-in"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-team">
                                    <div class="team-item">
                                        <div class="team-avater ajax-popup">
                                            <img src="<?php echo get_template_directory_uri();?>/assets/images/team/warisul-abid.jpg" alt="...">
                                        </div>
                                        <div class="team-denote">
                                            <h5 class="team-name">Warisul Abid</h5>
                                            <div class="team-designation">Chief People Officer, SQ Group</div>
                                            <ul class="team-socials">
                                                <li>
                                                    <a href="javascript:void()"><i class="fab fa-linkedin-in"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	
    <section class="section section-investor_block pt-0">
        <div class="container">
            <div class="investor_block">
                <h6 class="investor_block-title">OUR INVESTORS</h6>
                <ul class="investor_block-lists">
                    <?php foreach ($features as $feature): ?>
                    <li>
                        <div class="card-investor_block">
                            <div class="investor_block-image">
                                <img src="<?php echo $feature['image']; ?>" alt="">
                            </div>
                        </div>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </section>
<?php
get_footer();
?>