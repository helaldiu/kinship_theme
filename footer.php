    <footer class="footer footer-light bg-light-gredient">

<?php if(!is_page('signin')){?>
        <div class="footer-demo">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-lg-8 mx-auto text-center">
                        <div class="foo-get_request">
                            <h2 class="foo-get_request_heading">Make your workforce more effective, <div class="span-md-block">get started today</div></h2>
                            <a href="<?php echo get_permalink(get_page_by_path('request-demo'));?>" class="btn btn-primary btn-custom btn-lg btn-foo-request"> Request Demo </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php } ?>
        <div class="container"> 
        	<?php if(!is_page('signin')){?>
            <div class="row-footer">
                <div class="col-footer">
                    <div class="footer-item">
                       <h6 class="footer-title">Platform </h6>
                        <ul class="footer-list">
                            <li>
                                <a  class="link-disabled" href="connect.html">
                                    Connect
                                </a>
                            </li>
                            <li>
                                <a  class="link-disabled" href="engagement.html">
                                    Engagement
                                </a>
                            </li>
                            <li>
                                <a class="link-disabled" href="hr.html">
                                    HR
                                </a>
                            </li>
                            <li>
                                <a class="link-disabled" href="analytics.html">
                                    Analytics
                                </a>
                            </li>
                            <li>
                                <a class="link-disabled" href="pricing.html">
                                    Pricing
                                </a>
                            </li>
                        </ul> 
                    </div>
                </div>
                <div class="col-footer">
                    <div class="footer-item">
                       <h6 class="footer-title">Solutions </h6>
                        <ul class="footer-list">
                            <li>
                                <a class="link-disabled" href="apparel&textile.html">
                                    Apparel & Textile
                                </a>
                            </li>
                            <li>
                                <a class="link-disabled" href="manufacturing.html">
                                    Manufacturing
                                </a>
                            </li>
                            <li>
                                <a class="link-disabled" href="mining.html">
                                    Supply Chain
                                </a>
                            </li>       
                        </ul> 
                    </div>
                </div>
                <div class="col-footer">
                    <div class="footer-item">
                       <h6 class="footer-title">Resources </h6>
                        <ul class="footer-list">
                            <li>
                                <a class="link-disabled" href="help-center.html">
                                    Help Center
                                </a>
                            </li>
                            <li>
                                <a class="link-disabled" href="webinars.html">
                                    Blog
                                </a>
                            </li>
                        </ul> 
                    </div>
                </div>
                <div class="col-footer">
                    <div class="footer-item">
                       <h6 class="footer-title">About Us </h6>
                        <ul class="footer-list">
                            <li>
                                <a href="<?php echo get_permalink(get_page_by_path('about-kinship'));?>">
                                    About
                                </a>
                            </li>
							<li>
                                <a class="link-disabled" class="link-disabled" href="">
                                    Join Us
                                </a>
                            </li>
                            <li>
                                <a class="link-disabled" href="in-thenews.html">
                                    In the news
                                </a>
                            </li>
                            <li>
                                <a class="link-disabled" href="">
                                    Join Us
                                </a>
                            </li>
                        </ul> 
                    </div>
                </div>
                <div class="col-footer">
                    <div class="footer-item">
                       <h6 class="footer-title">Contact </h6>
                        <ul class="footer-list">
                            <li>
                                <a href="<?php echo get_permalink(get_page_by_path('request-demo'));?>">
                                    Request Demo
                                </a>
                            </li>
                            <li>
                                <a class="link-disabled" href="<?php echo get_permalink(get_page_by_path('contact-us'));?>">
                                    Contact Us
                                </a>
                            </li>
                        </ul> 
                    </div>
                </div>
            </div>
            <?php } ?>
            <hr class="footer-hr">
            <div class="row">
                <div class="col-md-4 order-md-2">
                    <ul class="list-inline footer-socials text-md-right">
                        <li class="list-inline-item">
                            <a href="https://www.facebook.com/kinshipai/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.linkedin.com/company/kinshipai" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-8">
                    <ul class="list-inline footer-nav">
                        <li class="list-inline-item foo-copyright"><span class="copyright">&copy; Kinship AI 2019. All Rights Reserved</span></li>
                        <li class="list-inline-item"><a href="<?php echo get_permalink(get_page_by_path('terms'));?>">Terms</a></li>
                        <li class="list-inline-item"><a href="<?php echo get_permalink(get_page_by_path('privacy-policy'));?>">Privacy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
<?php wp_footer();?>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/plugins/parallax/parallax.min.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/plugins/ripple/ripple.min.js"></script>

    <script src="<?php echo get_template_directory_uri();?>/assets/plugins/rellax.min.js"></script>

    <script src="<?php echo get_template_directory_uri();?>/assets/plugins/slick/slick.min.js"></script>

    <script src="<?php echo get_template_directory_uri();?>/assets/plugins/waypoint/jquery.waypoints.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/plugins/waypoint/sticky.min.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/plugins/metismenu/metisMenu.min.js"></script>

    <script>
        ! function() {
            "use strict";
            var a, b = document.getElementById("kutuHero"),
                c = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
                d = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
            d > 60 ? window.onscroll = function() {
                window.requestAnimationFrame(function() {
                    a = Number(void 0 !== window.pageYOffset ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop), b.style.opacity = 1 - a / c * .75
                })
            } : document.body.addEventListener("touchmove", function() {
                window.requestAnimationFrame(function() {
                    a = Number(void 0 !== window.pageYOffset ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop), b.style.opacity = 1 - a / c * 1.1
                })
            }, !1)
        }();

        $(document).ready(function() {

            var sticky = new Waypoint.Sticky({
              element: $('#secSupply')[0]
            });
			
			
			

             var bannerSlider = $('.home-screen-slider').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              autoplay: true,
              fade: true,
              asNavFor: '.home-screen-nav'
            });
            var bannerSliderNav = $('.home-screen-nav').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              asNavFor: '.home-screen-slider',
              vertical: true,
              adaptiveHeight: true,
              autoplay: true,
              arrows: false,
              dots: false,
              centerMode: false,
            });

        });
		<?php if(is_home()){?>
         $(window).on('load', function() {
            setTimeout(function(){
                var sticky = new Waypoint.Sticky({
                  element: $('#kutuHero')[0]
                });
                var rellax = new Rellax('.rellax');
           }, 1000);
         });
		<?php }?>

         $(document).ready(function() {
         var child_item = new Array($('.dropdown-item').attr("desc"));
         console.log(child_item)
        //  $.each(child_item, function (index, value) {
        //    console.log("dkjgh")
        // });
         
             
         });
    </script>
    <script type="text/javascript">
    //         if ($(window).width() > 768) {
    //    $('.dropdown-hover').hover(function() {
    //       $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(300);
    //     }, function() {
    //       $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(300);
    //     });
    // }

    if ($(window).width() >= 992) {
        $(window).scroll(function() {
            var sTop = $(window).scrollTop();
            if (sTop > 160) {
                $('#navbarDesktop').addClass('n-shadow bg-light');
                $('#navbarDesktop .navbar-brand img').attr('src', '<?php echo get_template_directory_uri();?>/assets/images/logo-icon.svg');
            } else {
                $('#navbarDesktop').removeClass('n-shadow bg-light');
                $('#navbarDesktop .navbar-brand img').attr('src', '<?php echo get_template_directory_uri();?>/assets/images/logo.svg');
            }
         });
    }
    if ($(window).width() <= 991) {
        $(window).scroll(function() {
            var sTop = $(window).scrollTop();
            if (sTop > 160) {
                $('#navbarMobile').addClass('n-shadow');
                $('#navbarMobile .navbar-brand img').attr('src', '<?php echo get_template_directory_uri();?>/assets/images/logo-icon.svg');
            } else {
                $('#navbarMobile').removeClass('n-shadow');
                $('#navbarMobile .navbar-brand img').attr('src', '<?php echo get_template_directory_uri();?>/assets/images/logo.svg');
            }
         });
    }
		
    if ($(window).width() >= 992) {
        $(window).scroll(function() {
            var sTop = $(window).scrollTop();
            if (sTop > 160) {
                $('#navbarDesktop').addClass('n-shadow bg-light');
                $('#navbarDesktop .navbar-brand img').attr('src', '<?php echo get_template_directory_uri();?>/assets/images/logo-icon.svg');
            } else {
                $('#navbarDesktop').removeClass('n-shadow bg-light');
                $('#navbarDesktop .navbar-brand img').attr('src', '<?php echo get_template_directory_uri();?>/assets/images/logo.svg');
            }
         });
    }
    if ($(window).width() <= 991) {
        $(window).scroll(function() {
            var sTop = $(window).scrollTop();
            if (sTop > 160) {
                $('#navbarMobile').addClass('n-shadow');
                $('#navbarMobile .navbar-brand img').attr('src', '<?php echo get_template_directory_uri();?>/assets/images/logo-icon.svg');
            } else {
                $('#navbarMobile').removeClass('n-shadow');
                $('#navbarMobile .navbar-brand img').attr('src', '<?php echo get_template_directory_uri();?>/assets/images/logo.svg');
            }
         });
    }

		
    </script>
    <script src="<?php echo get_template_directory_uri();?>/assets/js/script.js"></script>
</body>

</html>