<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Kinship: The Factory Workforce Platform</title>
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri();?>/assets/images/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri();?>/assets/images/favicon/favicon-16x16.png" sizes="16x16" />

    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" 
    integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/plugins/plugin.css">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/style.css">
    <?php wp_head(); ?>
</head>

<body>
    <nav id="navbarDesktop" class="navbar navbar-expand-lg fixed-top navbar-light navbar-transparent navbar-dropdown-center">
        <div class="container">
            <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
                <img src="<?php echo get_template_directory_uri();?>/assets/images/logo.svg" alt="Kinship">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navCollapse" aria-controls="navCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <?php wp_nav_menu(array(
                 'theme_location' => 'header_menu',
                 'container' => 'div',
                 'container_class' => 'collapse navbar-collapse',
                 'menu_class' => 'navbar-nav',
                 'depth' => '3',
                 'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                 //'walker' => new IBenic_Walker(),
                 'walker'            => new WP_Bootstrap_Navwalker()
                 
                ));


                ?>

            <div class="collapse navbar-collapse" id="navCollapse">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown dropdown-hover">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Platform
                        </a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-has-description">
                                <a class="dropdown-item" href="connect.html">
                                    <div class="dropdown-menu-item-block">
                                        <div class="dropdown-menu-item-media">
                                            <div class="dropdown-menu-item-media-icon">
                                                <img src="<?php echo get_template_directory_uri();?>/assets/images/nav/connect.svg" alt="">
                                            </div>
                                        </div>
                                        <div class="dropdown-menu-item-denote">
                                            <div class="nav-title">Connect</div>
                                            <div class="nav-description">
                                                Email alternative for better communication
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-has-description">
                                <a class="dropdown-item" href="engagement.html">
                                    <div class="dropdown-menu-item-block">
                                        <div class="dropdown-menu-item-media">
                                            <div class="dropdown-menu-item-media-icon">
                                                <img src="<?php echo get_template_directory_uri();?>/assets/images/nav/engage.svg" alt="">
                                            </div>
                                        </div>
                                        <div class="dropdown-menu-item-denote">
                                            <div class="nav-title">Engagement</div>
                                            <div class="nav-description">
                                                 Continuous engagement with chatbot
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-has-description">
                                <a class="dropdown-item" href="hr.html">
                                    <div class="dropdown-menu-item-block">
                                        <div class="dropdown-menu-item-media">
                                            <div class="dropdown-menu-item-media-icon">
                                                <img src="<?php echo get_template_directory_uri();?>/assets/images/nav/hr.svg" alt="">
                                            </div>
                                        </div>
                                        <div class="dropdown-menu-item-denote">
                                            <div class="nav-title">HR</div>
                                            <div class="nav-description">
                                                Reduce cost with self service HR
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-has-description">
                                <a class="dropdown-item" href="analytics.html">
                                    <div class="dropdown-menu-item-block">
                                        <div class="dropdown-menu-item-media">
                                            <div class="dropdown-menu-item-media-icon">
                                                <img src="<?php echo get_template_directory_uri();?>/assets/images/nav/analitycs.svg" alt="">
                                            </div>
                                        </div>
                                        <div class="dropdown-menu-item-denote">
                                            <div class="nav-title">Analytics</div>
                                            <div class="nav-description">
                                                Get insights that lead to action
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown dropdown-hover">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Solutions
                        </a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-has-no-description">
                                <a class="dropdown-item" href="solutions.html">
                                    <div class="dropdown-menu-item-block">
                                        <div class="dropdown-menu-item-media">
                                            <div class="dropdown-menu-item-media-icon">
                                                <img src="<?php echo get_template_directory_uri();?>/assets/images/nav/apparel.svg" alt="">
                                            </div>
                                        </div>
                                        <div class="dropdown-menu-item-denote">
                                            <div class="nav-title">Apparel & Textile</div>
                                        </div>
                                    </div>
                                </a>
                            </li>     
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.html">About</a>
                    </li>
                </ul>
                <ul class="navbar-nav nav-actions ml-auto">
                    <li class="nav-item">
                        <a class="nav-link nav-link-signin" href="signin.html">Sign In</a>
                    </li>
                    <li class="nav-item nav-item-btn">
                        <a class="nav-link nav-btn-demo" href="request-demo.html">Request Demo</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <nav id="navbarMobile" class="navbar bg-light navbar-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.html">
                <img src="<?php echo get_template_directory_uri();?>/assets/images/logo.svg" alt="Kinship">
            </a>
            <div class="mobile-metas">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navCollapseMobile" 
                aria-controls="navCollapseMobile" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            
            <div class="collapse navbar-collapse" id="navCollapseMobile">
                <ul id="mobileMenu">
                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">Platform</a>
                        <ul>
                            <li>
                                <a href="connect.html">
                                    Connect
                                </a>
                            </li>
                            <li>
                                <a href="engagement.html">
                                    Engagement
                                </a>
                            </li>
                            <li>
                                <a href="hr.html">
                                    HR
                                </a>
                            </li>
                            <li>
                                <a href="analytics.html">
                                    Analytics
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false">Solutions</a>
                        <ul>
                            <li>
                                <a href="solutions.html">
                                    Apparel & Textile
                                </a>
                            </li>     
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.html">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="request-demo.html">Request Demo</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="signin.html">Sign In</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
  
     