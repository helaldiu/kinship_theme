(function($, window, document, undefined) {
    'use strict';

    $.ripple(".btn", {
        debug: false, // Turn Ripple.js logging on/off
        on: 'mousedown', // The event to trigger a ripple effect

        opacity: 0.4, // The opacity of the ripple
        color: "auto", // Set the background color. If set to "auto", it will use the text color
        multi: false, // Allow multiple ripples per element

        duration: 0.7, // The duration of the ripple

        // Filter function for modifying the speed of the ripple
        rate: function(pxPerSecond) {
            return pxPerSecond;
        },

        easing: 'linear' // The CSS3 easing function of the ripple
    });

    init__dropdownHover();
    init__manufacturedImage();
    //init__animatedText();

    // $(window).scroll(function() {
    //     var sTop = $(window).scrollTop();
    //     if (sTop > 60) {
    //         $('.fixed-top').removeClass('navbar-dark').addClass('navbar-light bg-light shadow');
    //     } else {
    //         $('.fixed-top').removeClass('navbar-light bg-light shadow').addClass('navbar-dark');
    //     }
    // });

    /*if ($(window).width() > 768) {
       $('.dropdown-hover').hover(function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(300);
        }, function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(300);
        });
    }*/



    

    /* $(window).scroll(function() {
        var sTop = $(window).scrollTop();
        if (sTop > 160) {
            $('#navbarDesktop').addClass('n-shadow bg-light');
            $('#navbarMobile').addClass('n-shadow');
            $('.navbar-brand img').attr('src', 'assets/images/logo-icon.svg');
        } else {
            $('#navbarDesktop').removeClass('n-shadow bg-light');
            $('#navbarMobile').removeClass('n-shadow');
            $('.navbar-brand img').attr('src', 'assets/images/logo.svg');
        }
     }); */

    $("#mobileMenu").metisMenu({ toggle: false });
    /* $('#navCollapseMobile').on('show.bs.collapse', function () {
        $(this).closest('#navbarMobile').addClass('bg-light')
    });
    $('#navCollapseMobile').on('hidden.bs.collapse', function () {
        $(this).closest('#navbarMobile').removeClass('bg-light')
    }); */

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $('.smooth-scroll').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 60
                }, 400);
                return false;
            }
        }
    });



})(jQuery, window, document);


// TYPED ANIMATION
function init__animatedText() {
    if ($('#typed').length) {
        var typed = new Typed('#typed', {
            stringsElement: '#typed-strings',
            typeSpeed: 300,
            backSpeed: 50,
            backDelay: 3000,
            startDelay: 100,
            loop: true
        });
    }   
}


function init__dropdownHover() {
    const $dropdown = $(".dropdown-hover");
    const $dropdownToggle = $(".dropdown-toggle");
    const $dropdownMenu = $(".dropdown-menu");
    const showClass = "show";

    if (this.matchMedia("(min-width: 992px)").matches) {
        $dropdown.hover(
          function() {
            const $this = $(this);
            $this.addClass(showClass);
            $this.find($dropdownToggle).attr("aria-expanded", "true");
            $this.find($dropdownMenu).addClass(showClass);
          },
          function() {
            const $this = $(this);
            $this.removeClass(showClass);
            $this.find($dropdownToggle).attr("aria-expanded", "false");
            $this.find($dropdownMenu).removeClass(showClass);
          }
        );
      } else {
        $dropdown.off("mouseenter mouseleave");
    }
}

function init__manufacturedImage() {
    if(this.matchMedia("(min-width: 992px)").matches) {
        var ww = $(window).width(),
            cw = $(".container").outerWidth(),
            colw = $(".manufac-nag__image-column").outerWidth(),
            gw = (ww - cw) / 2,
            iw = (colw + gw) - 64;

            $('.manufac-nag-image-box').width(iw);
    }
}
