<?php
/*
Template Name: Service
*/
 get_header();?>

?>

<section class="section hero-pages-secondary d-flex align-items-center">
        <div class="container relative">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h1 class="hero-heading">
                        Apparel & Textile
                    </h1>
                    <div class="hero-text">
                        <p>
                            With Kinship, managing complex healthcare schedules for mobile providers is a breeze. 
                            Use Kinship’s home care scheduling software and mobile app to manage home healthcare delivery: appointment scheduling, advanced analytics, and more!
                        </p>
                    </div>
                </div>
                <div class="col-md-5 offset-md-1">
                    <img src="assets/images/demo/1.jpg" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </section>


    <section class="section">
        <div class="container">
            <div class="row manufac__flexbar manufac__flexbar-image-right">
                <div class="col-md-6 offset-md-1 order-md-2 manufac-nag__image-column">
                    <div class="manufac-nag-image-box">
                        <div class="manufac-nag-image">
                            <img src="assets/images/shots/connect_1.png" alt="...">
                        </div>
                    </div>
                </div>
                <div class="col-md-5 align-self-center">
                    <h2 class="manufac__flexbar-title">Reduce costs with intelligent healthcare scheduling</h2>
                    <div class="manufac__flexbar-text">
                        <p>
                            Kinship’s intelligent healthcare scheduling engine matches providers to patients based on your business objectives. Reduce costs by cutting travel time, clustering nearby visits, 
                            and eliminating missed appointments due to manual error. Manage schedules your way with shifts, rosters, calendars, and capacity scheduling.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="section pt-0">
        <div class="container">
            <div class="row">
                <div class="col-md-4 c-mb">
                    <div class="net-card card__blank text-left">
                        <div class="nc-icon">
                            <img src="assets/images/icons/Dashboard.svg" alt="...">
                        </div>
                        <div class="nc-denote">
                            <h5 class="nc-title">Improve retention</h5>
                            <div class="nc-text">
                                Engaged employees are more likely to stay with you for the long-term. Our customers have reduced their turnover by as much as 50%, saving hundreds of thousands of dollars in training and recruitment costs each year.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 c-mb">
                    <div class="net-card card__blank text-left">
                        <div class="nc-icon">
                            <img src="assets/images/icons/Training.svg" alt="...">
                        </div>
                        <div class="nc-denote">
                            <h5 class="nc-title">Reduce risk</h5>
                            <div class="nc-text">
                                An engaged workforce experiences 41% fewer safety incidents and 41% fewer quality defects. When you manage your culture effectively, it serves as an unseen force that encourages your employees to do the right thing, even when no one is looking.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 c-mb">
                    <div class="net-card card__blank text-left">
                        <div class="nc-icon">
                            <img src="assets/images/icons/Analytics.svg" alt="...">
                        </div>
                        <div class="nc-denote">
                            <h5 class="nc-title">Reach everyone</h5>
                            <div class="nc-text">
                                We can gather feedback from anyone, anywhere, anytime. Accessible via web, kiosk and SMS, and translated into over 50 languages - you can be certain every employee will have their voice heard.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="customer_story" class="section section-split-bg split-bg-w_80 d-flex align-items-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h6>CUSTOMER STORY</h6>
                    <h2>Making employee satisfaction the top priority at Autobutler</h2>
                    <p>
                        Autobutler was created to improve transparency in the automotive industry, yet prior to Peakon, transparency around employee engagement had not been an area of focus for the business. Co-founder, Peter Zigler explains "Peakon has literally changed how we're working with employee satisfaction at Autobutler. It has given us the ability to be much more detailed and sophisticated in our decision making
                    </p>

                    <h5 class="mt-5 mb-3">Results Achieved</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="achieved-item">
                                <div class="achieved-icon">
                                    <i class="far fa-check-circle"></i>
                                </div>
                                <p>
                                    <strong>Gathered</strong> employee feedback across remote locations worldwide
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="achieved-item">
                                <div class="achieved-icon">
                                    <i class="far fa-check-circle"></i>
                                </div>
                                <p>
                                    <strong>Identified</strong> strengths and priorities for teams, regions and the organisation as a whole
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="achieved-item">
                                <div class="achieved-icon">
                                    <i class="far fa-check-circle"></i>
                                </div>
                                <p>
                                    <strong>Provided</strong> real-time insights to managers throughout the business
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="achieved-item">
                                <div class="achieved-icon">
                                    <i class="far fa-check-circle"></i>
                                </div>
                                <p>
                                    <strong>Built</strong> a successful and unified company culture across multiple locations
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 offset-md-1">
                    <img src="assets/images/demo/autobutler-video-3d.webp" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-6 mx-auto text-center">
                    <h2 class="color-primary mb-4">The many ways Peakon can support your organisation</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 c-mb">
                    <div class="organisation-item">
                        <div>
                            <div class="organisation-icon">
                                <img src="assets/images/icons/Dashboard.svg" alt="">
                            </div>   
                        </div>
                        <div class="organisation-denote">
                            <h5>Employee Engagement</h5>
                            <div class="organisation-text">
                               Increase productivity, retention and business performance with Peakon 
                            </div>
                            <a href="" class="text-link">Learn more</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 c-mb">
                    <div class="organisation-item">
                        <div>
                            <div class="organisation-icon">
                                <img src="assets/images/icons/Training.svg" alt="">
                            </div>   
                        </div>
                        <div class="organisation-denote">
                            <h5>Employee Engagement</h5>
                            <div class="organisation-text">
                               Increase productivity, retention and business performance with Peakon 
                            </div>
                            <a href="" class="text-link">Learn more</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 c-mb">
                    <div class="organisation-item">
                        <div>
                            <div class="organisation-icon">
                                <img src="assets/images/icons/Analytics.svg" alt="">
                            </div>   
                        </div>
                        <div class="organisation-denote">
                            <h5>Employee Engagement</h5>
                            <div class="organisation-text">
                               Increase productivity, retention and business performance with Peakon 
                            </div>
                            <a href="" class="text-link">Learn more</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 c-mb">
                    <div class="organisation-item">
                        <div>
                            <div class="organisation-icon">
                                <img src="assets/images/icons/Analytics.svg" alt="">
                            </div>   
                        </div>
                        <div class="organisation-denote">
                            <h5>Employee Engagement</h5>
                            <div class="organisation-text">
                               Increase productivity, retention and business performance with Peakon 
                            </div>
                            <a href="" class="text-link">Learn more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="section_globe" class="section section-split-bg split-bg-w_55 d-flex align-items-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <h2>Powering Manufacturing & Construction businesses around the globe</h2>
                </div>
                <div class="col-md-8">
                    <div class="industry__logos-container">
                        <div class="industry__logo">
                            <img src="assets/images/demo/bovis@2x.webp" alt="">
                        </div>
                        <div class="industry__logo">
                            <img src="assets/images/demo/bovis@2x.webp" alt="">
                        </div>
                        <div class="industry__logo">
                            <img src="assets/images/demo/bovis@2x.webp" alt="">
                        </div>
                        <div class="industry__logo">
                            <img src="assets/images/demo/bovis@2x.webp" alt="">
                        </div>
                        <div class="industry__logo">
                            <img src="assets/images/demo/bovis@2x.webp" alt="">
                        </div>
                        <div class="industry__logo">
                            <img src="assets/images/demo/bovis@2x.webp" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer class="footer footer-light bg-light-gredient">
        <div class="footer-demo">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-lg-8 mx-auto text-center">
                        <div class="foo-get_request">
                            <h2 class="foo-get_request_heading">Make your workers more effective, <div class="span-md-block">get started today</div></h2>
                            <a href="request-demo.html" class="btn btn-primary btn-custom btn-lg btn-foo-request"> Request Demo </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container"> 
            <div class="row-footer">
                <div class="col-footer">
                    <div class="footer-item">
                       <h6 class="footer-title">Platform </h6>
                        <ul class="footer-list">
                            <li>
                                <a href="connect.html">
                                    Connect
                                </a>
                            </li>
                            <li>
                                <a href="engagement.html">
                                    Engagement
                                </a>
                            </li>
                            <li>
                                <a href="hr.html">
                                    Mobile HR
                                </a>
                            </li>
                            <li>
                                <a href="training.html">
                                    Training
                                </a>
                            </li>
                            <li>
                                <a href="compliance.html">
                                    Compliance
                                </a>
                            </li>
                            <li>
                                <a href="sms-kiosks-sensors.html">
                                    SMS, Kiosks & Sensors
                                </a>
                            </li>
                            <li>
                                <a href="analytics.html">
                                    Analytics
                                </a>
                            </li>
                            <li>
                                <a href="pricing.html">
                                    Pricing
                                </a>
                            </li>
                        </ul>  
                    </div>
                </div>
                <div class="col-footer">
                    <div class="footer-item">
                       <h6 class="footer-title">Solutions </h6>
                        <ul class="footer-list">
                            <li>
                                <a href="apparel&textile.html">
                                    Apparel & Textile
                                </a>
                            </li>
                            <li>
                                <a href="manufacturing.html">
                                    Manufacturing
                                </a>
                            </li>
                            <li>
                                <a href="mining.html">
                                    Mining
                                </a>
                            </li>
                            <li>
                                <a href="agriculture.html">
                                    Agriculture
                                </a>
                            </li>        
                        </ul> 
                    </div>
                </div>
                <div class="col-footer">
                    <div class="footer-item">
                       <h6 class="footer-title">Resources </h6>
                        <ul class="footer-list">
                            <li>
                                <a href="help-center.html">
                                    Help Center
                                </a>
                            </li>
                            <li>
                                <a href="webinars.html">
                                    Webinars
                                </a>
                            </li>
                        </ul> 
                    </div>
                </div>
                <div class="col-footer">
                    <div class="footer-item">
                       <h6 class="footer-title">About Us </h6>
                        <ul class="footer-list">
                            <li>
                                <a href="team.html">
                                    Team
                                </a>
                            </li>
                            <li>
                                <a href="careers.html">
                                    Careers
                                </a>
                            </li>
                            <li>
                                <a href="in-thenews.html">
                                    In the news
                                </a>
                            </li>
                            <li>
                                <a href="partners.html">
                                    Partners
                                </a>
                            </li>
                        </ul> 
                    </div>
                </div>
                <div class="col-footer">
                    <div class="footer-item">
                       <h6 class="footer-title">Contact </h6>
                        <ul class="footer-list">
                            <li>
                                <a href="request-demo.html">
                                    Request Demo
                                </a>
                            </li>
                            <li>
                                <a href="platform-support.html">
                                    Platform Support
                                </a>
                            </li>
                            <li>
                                <a href="contact-us.html">
                                    Contact Us
                                </a>
                            </li>
                        </ul> 
                    </div>
                </div>
            </div>
            <hr class="footer-hr">
            <div class="row">
                <div class="col-md-4 order-md-2">
                    <ul class="list-inline footer-socials text-md-right">
                        <li class="list-inline-item">
                            <a href="https://www.facebook.com/Kinship/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.linkedin.com/company/Kinship" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://twitter.com/Kinship" target="_blank"><i class="fab fa-twitter"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-8">
                    <ul class="list-inline footer-nav">
                        <li class="list-inline-item foo-copyright"><span class="copyright">&copy; Kinship AI 2019. All Rights Reserved</span></li>
                        <li class="list-inline-item"><a href="terms.html">Terms</a></li>
                        <li class="list-inline-item"><a href="privacy-policy.html">Privacy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

<?php get_footer();?>