<?php
/*
Template Name: Privacy Policy 
*/
 get_header();?>

<section class="section">
        <div class="container meta-text-widget">
            <h1 class="meta-page-hero-heading">Privacy Policy</h1>
            <p>
                This privacy policy (“Privacy Policy”) provides our policies and procedures for collecting, using, and disclosing your information. Users can access the services provided by Kinship Pte Ltd (“Kinship”, “we” or “our”) through our website
                <a href="https://kinship.ai/">www.kinship.ai</a> (the “Services”). This Privacy Policy governs your access of the Services, regardless of what type of device or application you use to access them. By using our Services you consent to the collection, transfer, processing, storage, disclosure and other uses of information described in this Privacy Policy. The term “Information” refers to all of the different forms of data, content, and information described below.
            </p>
            <h5>1. The Information We Collect and Store</h5>
            <p>
                We may collect and store the following Information when you are using the Services:
            </p>
            <ul class="list-styled-bulets-circle">
                <li>
                    <strong>INFORMATION YOU PROVIDE</strong>
                    <br>When you register for, or access, a Kinship account, we may collect some personal Information that can be used to contact or identify you (“Personal Information”), such as your name, phone number, credit card or other billing Information, email address, home and business postal addresses.
                </li>
                <li>
                    <strong>INFORMATION YOUR EMPLOYER MAY PROVIDE</strong>
                    <br> The Services may also be used by your employer to store other certain employment-related Information (“Employment Information”). This Employment Information may include employment status, benefits information and emergency contacts. This Employment Information is encrypted and stored on secured servers and may only be accessed by a person or persons designated by your employer as an account administrator. Kinship does not access your Employment Information, nor do the Services allow for dissemination of such Information other than by you, the human resource administrators of the account, or the owner(s) of the account (each referred to herein as an “account administrator”), or as may be required by law.
                </li>
                <li>
                    <strong>FILES</strong>
                    <br> We collect and store the files you or your account administrator uploads, downloads, or accesses with the Services (“Files”). If you or your account administrator adds a File to a Kinship account that has been previously uploaded by you or another user, we may associate all or a portion of the previous File with your account rather than storing a duplicate.
                </li>
                <li>
                    <strong>COOKIES</strong>
                    <br> We use “cookies” to collect Information and improve our Services. A cookie is a small data file that we transfer to your device. We may use “session ID cookies” to enable certain features of the Services, to better understand how you interact with the Services and to monitor aggregate usage and web traffic routing on the Services. We may also use “persistent cookies” to save your registration ID and login password for future logins to the Services. You can instruct your browser, by changing its options, to stop accepting cookies or to prompt you before accepting a cookie from the websites you visit. If you do not accept cookies, however, you may not be able to use all aspects of the Services.
                </li>
                <li>
                    <strong>LOG DATA</strong>
                    <br> When you use the Services, we automatically record Information from the device you use, its software, and your activity using the Services. This may include the device’s Internet Protocol (“IP”) address, browser type, the web page(s) visited before you came to our website, Information you search for on our website, locale preferences, identification numbers associated with your devices, your mobile carrier, date and time stamps associated with transactions, system configuration, metadata concerning your Files, and other interactions with the Services.
                </li>
                <li>
                    <strong>INTEGRATIONS</strong>
                    <br> Kinship may allow third party integrators (“Integrators“) to create applications and/or tools that supplement or enhance the Services (the “Integrations“). If you choose to access any such Integrations, the Integrators will access (via API) and use information you provide solely for the purpose of supplementing or enhancing the Services through the Integrations.
                </li>
            </ul>
            <h5>2. How We Use Your Information</h5>
            <ul class="list-styled-bulets-circle">
                <li>
                    <strong>PERSONAL INFORMATION</strong>
                    <br>Personal Information and data collected from cookies and other logging data may be used: (i) to provide and improve our Services and customer service, (ii) to administer your use of the Services, (iii) to better understand your needs and interests, (iv) to personalize and improve your experience, and (v) to provide or offer software updates and product announcements. If you no longer wish to receive communications from us, please follow the “unsubscribe” instructions provided in any of those communications, or update your account settings, as applicable. We do not sell your Personal Information, Files or Employment Information to third parties.
                </li>
                <li>
                    <strong>EMPLOYMENT INFORMATION AND FILES</strong>
                    <br> Employment Information and Files are encrypted and stored with Amazon’s S3 storage service and may be accessed and downloaded by you and your account administrator(s). Kinship does not access or use Employment Information or your Files other than (i) in an encrypted form or (ii) in aggregated reports that do not contain, nor that can be used to extract, personally identifiable information. Your account administrator is responsible for any use of your personal Employment Information or Files.
                </li>
                <li>
                    <strong>ANALYTICS</strong>
                    <br> We may also collect some Information (ourselves or by using third party services) using logging and cookies, such as IP addresses, which can sometimes be correlated with Personal Information. We use this Information for the above purposes and to monitor and analyze use of the Services, for the Services’ technical administration, to increase our Services’ functionality and user-friendliness, and to verify users have the authorization needed for the Services to process their requests.
                </li>
            </ul>
            <h5>3. Information Sharing and Disclosure</h5>
            <ul class="list-styled-bulets-circle">
                <li>
                    <strong>YOUR USE</strong>
                    <br>We will display your Personal Information and some Employment Information in your profile page and any profile page created by your account administrator and elsewhere on the Services according to the preferences you or your account administrator sets in your profile or in any Kinship account which you access. You or your account administrator can review and revise your profile Information at any time.
                </li>
                <li>
                    <strong>SERVICE PROVIDERS, BUSINESS PARTNERS AND OTHERS</strong>
                    <br> We do not sell your Personal Information, Files or Employment Information to third parties. We may use certain trusted third party companies and individuals to help us provide, analyze, and improve the Services (including but not limited to data storage, maintenance services, database management, web analytics, payment processing, and improvement of the Services’ features). These third parties may have access to your Information only for purposes of performing these limited tasks on our behalf and under obligations similar to those in our Privacy Policy. We use Amazon’s S3 storage service to store your Files and other Information.
                </li>
                <li>
                    <strong>COMPLIANCE WITH LAWS AND LAW ENFORCEMENT REQUESTS</strong>
                    <br> We may disclose to outside parties Files, Personal Information and/or Employment Information stored in Kinship accounts which you access and Information about you that we collect when we have a good faith belief that disclosure is reasonably necessary to (a) comply with a law, regulation or compulsory legal request; (b) protect the safety of any person from death or serious bodily injury; (c) prevent fraud or abuse of Kinship, its Services or its users; or (d) to protect Kinship’s property rights. If we provide your Files to a law enforcement agency as set forth above, we will remove Kinship’s encryption from the Files before providing them to law enforcement. However, Kinship will not be able to access and decrypt any Files on your behalf or that you encrypted prior to storing them via the Services.
                </li>
                <li>
                    <strong>TRANSFERS OF OUR BUSINESS</strong>
                    <br> If we are involved in a merger, acquisition, or sale of all or a portion of our assets, your Information may be transferred as part of that transaction, but we will notify you (for example, via email and/or a prominent notice on our website) of any change in control or use of your Personal Information, Employment Information or Files, or if such Information becomes subject to a different privacy policy.
                </li>
                <li>
                    <strong>NON-PRIVATE OR NON-PERSONAL INFORMATION</strong>
                    <br> We may, at our discretion, disclose your non-private, aggregated, or otherwise non-personal Information, such as usage statistics of our Services.
                </li>
                <li>
                    <strong>OPTING OUT </strong>
                    <br>Other than the primary elements of the Services that we encrypt and provide through Amazon’s S3 storage, you may opt out of our using or sharing any other Personal Information with our third party business partners. Opting out may affect how well we are able to service your account(s) and customer needs, but we will still be able to provide you with the Services. You may opt out by selecting the applicable “opt out” features within the Services, or contacting us via email at <a href="mailTo:hello@Kinship.ai"><span>hello@kinship.ai</span></a>.
                </li>
            </ul>
            <h5>4. Changing or Deleting Your Information</h5>
            <p>
                If you are registered as the account holder or account administrator, you may review, update, correct or delete the Personal Information provided in your registration or account profile by changing the “account settings.” Files and Employment Information may only be updated or deleted by your account administrator. In some cases we may retain copies of your Information and Files if required by law. For questions about your Personal Information, Employment Information or Files on our Services, please contact <a href="mailTo:hello@Kinship.ai"><span class="__cf_email__">hello@kinship.ai</span></a>. We will respond to your inquiry within 30 days.
            </p>
            <h5>5. Data Retention</h5>
            <p>
                We may retain and use your Information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements. Consistent with these requirements, we will attempt to delete your Information quickly upon request by you or your account administrator, as applicable. Please note, however, that there might be latency in deleting Information from our servers and backed-up versions might exist after deletion. In addition, we do not delete from our servers Files that you have in common with other users.
            </p>
            <h5>6. Account and Administrator</h5>
            <p>
                Your account administrator has the ability to:
            </p>
            <ul class="list-styled-bulets-circle">
                <li>
                    access, add, delete modify or save all Employment Information, Files and other Information in and about your Kinship account;
                </li>
                <li>disclose, restrict, or access Information that you have provided or that is made available to you when using such Kinship account; and</li>
                <li>control how your Kinship account may be accessed, modified or deleted.</li>
            </ul>
            <p>
                Please refer to your organization’s policies if you have questions about your account administrator’s rights.
            </p>
            <h5>7. Security</h5>
            <p>
                The security of your Information is important to us. When you enter sensitive Information (such as a bank routing number) on our order forms and when your account administrator adds or uploads Employment Information or Files, we encrypt the transmission of that Information using secure socket layer technology (SSL).
            </p>
            <p>
                We follow generally accepted standards to protect the Information submitted to us, both during transmission and once we receive it. No method of electronic transmission or storage is 100% secure, however. Therefore, we cannot guarantee its absolute security. If you have any questions about security on our website, you can contact us at <a href="mailTo:hello@Kinship.ai"><span>hello@kinship.ai</span></a>.
            </p>
            <h5>8. Contacting Us</h5>
            <p>
                If you have any questions about this Privacy Policy, please contact us at <a href="mailTo:hello@Kinship.ai"><span>hello@kinship.ai</span></a>
            </p>
            <h5>9. Changes to our Privacy Policy</h5>
            <p>
                This Privacy Policy may change from time to time. If we make a change to this Privacy Policy that we believe materially reduces your rights, we will provide you with notice by email or by posting such changes on our site. We may provide notice of changes in other circumstances as well. By continuing to use the Services after those changes become effective, you agree to be bound by the revised Privacy Policy.
            </p>
        </div>
    </section>

<?php get_footer();?>