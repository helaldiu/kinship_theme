<?php
/*
Template Name: Help Center
*/
get_header();
?>

    <section class="section hero-pages-default d-flex align-items-center">
        <div class="container relative">
            <div class="row">
                <div class="col-md-11 col-lg-8 mx-auto text-center">
                    <h1 class="hero-heading">
                        <?php echo get_post_meta(get_the_ID(),'subtitle', true) ?>
                    </h1>
                </div>
                <div class="col-md-12 col-lg-8 mx-auto text-center">
                    <form action="" class="hero-page-form">
                        <div>
                            <input type="text" class="form-control" placeholder="Search for articles...">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="section pt-0 section-blog">
        <div class="container">
            <div class="row">
                <div class="col-md-11 col-lg-10 col-xl-9 mx-auto">

                    <?php
                    $blog = new WP_Query(array('post_type' => 'post', 'posts_per_page' => 4, 'order' => 'asc'));
                    if ($blog->have_posts()) :
                        while ($blog->have_posts()) :
                            $blog->the_post();
                            $author = get_the_author_meta('ID');
                            ?>

                            <div class="c__help">
                                <a href="javascript:void(0)" class="c__help_link">
                                    <div class="row">
                                        <div class="col-md-4 col-lg-3">
                                            <div class="help-icon">
                                                <img src="<?php echo get_template_directory_uri();?>/assets/images/faq.png" alt="">
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-9">
                                            <div class="help-content">
                                                <h5 class="help-name"><?php echo get_the_title(); ?></h5>
                                                <div class="help-text">
                                                    <?php echo cut_limit(get_the_content(),30);  ?>
                                                </div>
                                                <div class="help-avatar">
                                                    <div class="avatar__photo avatars__images o__ltr">
                                                        <img src="<?php echo get_avatar_url($author); ?>" alt="" class="avatar__image">
                                                    </div>
                                                    <div class="avatar__info">
                                                        <div>
                                                    <span class="c__darker">
                                                        <?php echo count_user_posts($author, 'post', true); ?> articles in this collection
                                                    </span>
                                                            <br>
                                                            Written by <span class="c__darker"> <?php echo get_the_author(); ?> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php
                        endwhile;
                    endif;

                    ?>

                </div>
            </div>
        </div>
    </section>
<?php
get_footer();
?>