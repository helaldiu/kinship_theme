<?php get_header(); ?>
    <div class="body-padding-topHome"></div>
<!--     <section id="kutuHero" class="section hero-light hero-demure">
        <div class="hero-demure-bg" style="background-image: url('<?php //echo ot_get_option('home_banner_photo');?>')"></div>
        <div class="container relative">
            <div class="row">
                <div class="col-md-6 col-lg-6 mx-auto text-center">
                    <h1 class="hero-heading"><?php //echo ot_get_option('home_banner_title');?></h1>
                    <div class="hero-subtext">
                        <p>
                            <?php // echo ot_get_option('home_banner_subtitle');?>
                        </p>
                    </div>
                    <div class="hero-actions text-center">
                        <a href="<?php //echo get_permalink(get_page_by_path('request-demo'));?>" class="btn btn-primary">Request Demo</a>
                    </div>
                </div>
            </div>
        </div>
        <img class="hero-mask" src="<?php //echo get_template_directory_uri();?>/assets/images/hero_mask.svg" alt="">
    </section>
 -->

<?php 
	$rd = get_post(ot_get_option('rd_page'),ARRAY_A);
?>


 <section id="kutuHero" class="section hero-light hero-home hero-home-unique">
        <div class="container relative">
            <div class="row align-items-md-center">
                <div class="col-md-5">
                    <h1 class="hero-heading">
                        <div class="home-screen-nav-sliders">
                            <div class="home-screen-nav">
								<?php 
								$homeslider=ot_get_option('home_banner_slider');
								if(count($homeslider)>0){
								foreach($homeslider as $slide){ ?>
                                <div class="slide-item"><?php echo $slide['title'];?></div>
                               <?php 
									}
								}
								?>
								
                            </div>
                        </div>
                        for the
                        factory workforce
                    </h1>  
                    <div class="hero-subtext pr-lg-5">
                        <p>
                            Using AI to manage workers, improve productivity and be more transparent in the supply chain. 
                        </p>
                    </div>
                    <div class="hero-actions">
                        <a href="<?php echo get_permalink($rd['ID']);?>" class="btn btn-primary btn-hero"><?php echo $rd['post_title']; ?></a>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="hero-home-screens-wrap">
                        <div class="hero-home-screens">
                            <div class="hero-home-screen-sliders">
                                <div class="home-screen-slider">
									
									<?php 
								
								if(count($homeslider)>0){
								foreach($homeslider as $slide){ ?>
                               
									 <div class="slide-item">
                                        <div class="home-screen-slide-image">
                                            <img src="<?php echo $slide['image'];?>" alt="">
                                        </div>
                                    </div>
									
                               <?php 
									}
								}
								?>
                                   
                                    
									
									
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>





    <section class="section bg-white s-has_hero_curved">
        <div class="container">
            <h2 class="section-heading color-primary text-center"><?php echo ot_get_option('home_about_title');?></h2>
        </div>
       <?php 
$home_about_lists=ot_get_option('home_page_about_section');
$count=0;
foreach($home_about_lists as $home_about_list){ 
    $count ++;
    if($count%2==0) {?>
<div <?php if($count==count($home_about_lists)){ echo  'id="secSupply" class="s-manager_stuck"';} ?> >
            <div class="container">
                <div class="featured-roof">
                    <div class="row">
                        <div class="col-md-5 offset-md-1 order-md-2">
                            <img src="<?php echo $home_about_list['image'];?>" alt="..." class="img-fluid">
                        </div>
                        <div class="col-md-5 offset-md-1 align-self-center">
                            <div class="roof-note">
                                <h2 class="roof-title"><?php echo $home_about_list['title'];?></h2>
                                <div class="roof-text para-lead_desktop">
                                    <p><?php echo $home_about_list['desc'];?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
   <?php  }else{?> 

  <div <?php if($count==count($home_about_lists)){ echo  'id="secSupply" class="s-manager_stuck"';} ?> >
            <div class="container">
                <div class="featured-roof">
                    <div class="row">
                        <div class="col-md-5 offset-md-1">
                            <img src="<?php echo $home_about_list['image'];?>" alt="..." class="img-fluid">
                        </div>
                        <div class="col-md-5 offset-md-1 align-self-center">
                            <div class="roof-note">
                                <h2 class="roof-title"><?php echo $home_about_list['title'];?></h2>
                                <div class="roof-text para-lead_desktop">
                                    <p><?php echo $home_about_list['desc'];?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   <?php }
    ?>
<?php }
        ?>
                  
    </section>

    <section id="goChat" class="section bg-white has-col-mb">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ml-auto mr-auto text-center header-group">
                    <h2 class="section-heading color-primary net-heading"><?php echo ot_get_option('home_benifits_title');?>  </h2>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">

                    	<?php 
                    	$allbenifets=ot_get_option('home_page_benifits_section');

                    	foreach ($allbenifets as $benifets) {?>
                    		
<div class="col-md-4 c-mb">
                            <div class="net-card">
                                <div class="nc-icon nc_icon-lg">
                                    <img src="<?php echo $benifets['image'];?>" alt="...">
                                </div>
                                <div class="nc-denote">
                                    <h5 class="nc-title"><?php echo $benifets['title'];?></h5>
                                    <div class="nc-text"><?php echo $benifets['desc'];?></div>
                                </div>
                            </div>
                        </div>

                    	<?php }

                    	?>
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section bg-white section-customer-success pt-0">
        <div class="container">
            <div class="row align-items-md-center">
                <div class="col-md-6 col-lg-7 col-xl-6 offset-xl-1 order-md-2">
                    <div class="i__connect_image">

                        <img class="img-fluid" src="<?php echo ot_get_option('platform_image');?>" alt="...">
                    </div>
                </div>
                <div class="col-md-6 col-lg-5 col-xl-5">
                    <div class="header-group">
                        <h2 class="section-heading color-primary"><?php echo ot_get_option('platform_title');?></h2>
                    </div>
                    <div>

                        <?php 
                        $allcont_platforms=ot_get_option('platform_content');
                        foreach($allcont_platforms as $allcont_platform){ ?>

 <div class="mission-item">
                            <h3 class="mi-counter"><?php echo  $allcont_platform['desc'];?></h3>
                            <div class="mi-label"><?php echo  $allcont_platform['title'];?></div>
                        </div>

                     <?php   }
                        ?>
                       
                        
                    </div>
                </div>  
            </div>
        </div>
    </section>
    
    <section  class="section bg-light-gredient section-integrations">
        <div class="container relative">
            <div class="row">
                <div class="col-md-6 col-lg-5 align-self-center integrations-content">
                    <div class="pr-3">
                        <h2 class="section-heading color-primary"><?php echo ot_get_option('home_brand_title');?></h2>
                        <div class="para-lead_desktop">
                            <p>
                                <?php echo ot_get_option('home_brand_desc');?>
                            </p>
                        </div>
                        
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="logo-bar-vertical">
                        <img src="<?php echo ot_get_option('home_brand_image');?>" alt="..."  class="img-fluid rellax img-brand-integration" data-rellax-speed="7" data-rellax-percentage="0.5">
                    </div>
                </div>
            </div>
        </div>
        <div class="gradient-fade"></div>
    </section>

    <!-- <section class="section bg-darkblue on-dark relative section-get-started">
        <div class="section-get-started-bg">
            <img class="get-started-bg-image" src="assets/images/footer-image.svg" alt="...">
        </div>
        <div class="container relative">
            <div class="text-center">
                <div class="get-col">
                    <h2>Make your workers more effective, <div class="span-md-block">get started today</div></h2>
                    <a href="request-demo.html" class="btn btn-primary btn-foo-request"> Request Demo </a>
                </div>
            </div>
        </div>
    </section> -->
<?php get_footer();?>