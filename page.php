<?php get_header();?>

	<?php if(have_posts()) : ?>
					<?php while (have_posts()) : the_post();

  $post_thumbnail_id = get_post_thumbnail_id(get_the_ID());
                        $post_thumbnail_src = wp_get_attachment_image_src( $post_thumbnail_id,'full'); //get thumbnail image url          
                        $image_src = $post_thumbnail_src[0];
						?>
        



    <section class="section hero-pages-default d-flex align-items-center">
        <div class="container relative">
            <div class="row">
                <div class="col-md-11 col-lg-9 mx-auto text-center">
                    <h1 class="hero-heading">
                       <?php the_title();?>
                    </h1>
                </div>
                <div class="col-md-12 col-lg-8 mx-auto text-center">
                    <div class="hero-text">
                        <p>
                           <?php the_content();?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php if($image_src){?>
    <section class="section pt-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center shot-border-shadow">
                        <img src="<?php echo  $image_src; ?>" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } 

$allfeatures=get_post_meta(get_the_ID(),'features',true);
if(count($allfeatures)>0){
?>

    <section class="section has-col-mb">
        <div class="container">
            <div class="row">

<?php 


foreach($allfeatures as $features){
	?>

<div class="col-md-3 c-mb">
                    <div class="management-item text-center">
                        <div class="management-icon m_icon-lg">
                            <img src="<?php echo $features['image'];?>" alt="News">
                        </div>
                        <div class="management-denote">
                            <h5 class="management-title"><?php echo $features['title'];?></h5>
                            <div class="management-text">
                                <p>
                                   <?php echo $features['desc'];?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

<?php }


?>

               
            </div>
        </div>
    </section>
<?php 
}
?>

<?php 
$allspecial=get_post_meta(get_the_ID(),'special_ins',true);

if(count($allspecial)>0){ 
?>
    <section class="section">
        <div class="container">
<?php 

$allspecial_list_view=get_post_meta(get_the_ID(),'special_order_type',true);
if(count($allspecial)>0){ 
	$count=0;
foreach($allspecial as $special){

$count++;
if($allspecial_list_view=='flat'){
	?>






            <div class="row flow-item ">
                <div class="col-md-6 col-lg-6 offset-lg-1 order-md-2">
                    <div class="flow-image shot-border-shadow">
                        <img src="<?php echo $special['image'];?>" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-6 col-lg-5 align-self-center">
                    <div class="flow-content">
                        <div class="flow-header">
                            <h3 class="flow-heading">
                                <?php echo $special['title'];?>
                            </h3>
                        </div>
                        <ul class="flow-lists">
                            <li>
                                <p><?php echo $special['desc'];?></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
<?php }elseif($allspecial_list_view=='numaric'){?>

 <div class="row flow-item -has-step-number">
                <div class="col-md-6 order-md-2">
                    <div class="flow-image shot-border-shadow">
                        <img src="<?php echo $special['image'];?>" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="flow-content">
                        <div class="flow-big-number"><?php echo $count; ?></div>
                        <div class="flow-header">
                            <h3 class="flow-heading">
                              <?php echo $special['title'];?>
                            </h3>
                        </div>
                        <ul class="flow-lists">
                            <li>
                                <p><?php echo $special['desc'];?></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

<?php }elseif($allspecial_list_view=='odd_even'){

	?>
<div class="row flow-item">
                <div class="col-md-6 col-lg-6 <?php if($count%2==0){}else{ echo 'offset-lg-1 order-md-2';}?> ">
                    <div class="flow-image shot-border-shadow">
                        <img src="<?php echo $special['image'];?>" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-6 col-lg-5 <?php if($count%2==0){echo 'offset-lg-1 align-self-center';}else{ echo 'align-self-center';}?> ">
                    <div class="flow-content">
                        <div class="flow-header">
                            <h3 class="flow-heading">
                               <?php echo $special['title'];?>
                            </h3>
                        </div>
                        <ul class="flow-lists">
                            <li>
                                <p><?php echo $special['desc'];?></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>



<?php }else{?>


<div class="row flow-item">
                <div class="col-md-6 col-lg-6 offset-lg-1 order-md-2">
                    <div class="flow-image shot-border-shadow">
                        <img src="<?php echo $special['image'];?>" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-6 col-lg-5 align-self-center">
                    <div class="flow-content">
                        <div class="flow-header">
                            <h3 class="flow-heading">
                                <?php echo $special['title'];?>
                            </h3>
                        </div>
                        <ul class="flow-lists">
                            <li>
                                <p><?php echo $special['desc'];?></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>


<?php } ?>

<?php }}


?>


        </div>
    </section>


<?php } ?>




            <?php endwhile; ?><?php endif; ?>

  <?php wp_reset_query();?>
        
<?php get_footer();?>